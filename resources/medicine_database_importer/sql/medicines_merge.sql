-- ************************************************************
-- SQLite Nerve Medicine database importer
-- 
-- Import data from csv files:
-- NPL  - Nationellt Produktregister för Läkemedel (SE)
-- NDC - National Drug Code (US)
--
-- ************************************************************

BEGIN TRANSACTION;

-- Import npl_medicine
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `npl_medicines`;

CREATE TABLE `npl_medicines` (
  `nplid` bigint(11) DEFAULT NULL,
  `tradename` varchar(128) DEFAULT NULL,
  `strength_group_name` varchar(32) DEFAULT NULL,
  `pharmaceutical_form_group_lx` varchar(3) DEFAULT NULL,
  `pharmaceutical_form_lx` varchar(6) DEFAULT NULL,
  `organization_lx` varchar(24) DEFAULT NULL
);
.separator \t
.import source/npl_medicine.csv npl_medicines

-- Import npl_organization
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `npl_organizations`;

CREATE TABLE `npl_organizations` (
  `organization_lx` varchar(32) DEFAULT NULL,
  `organization` varchar(128) DEFAULT NULL
);
.separator \t
.import source/npl_organization.csv npl_organizations

-- Import npl_pharmaceutical_form_group
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `npl_pharmaceutical_form_groups`;

CREATE TABLE `npl_pharmaceutical_form_groups` (
  `pharmaceutical_form_group_lx` varchar(32) DEFAULT NULL,
  `sv` varchar(128) DEFAULT NULL,
  `en` varchar(128) DEFAULT NULL
);
.separator \t
.import source/npl_pharmaceutical_form_group.csv npl_pharmaceutical_form_groups


-- Import npl_pharmaceutical_forms
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `npl_pharmaceutical_forms`;

CREATE TABLE `npl_pharmaceutical_forms` (
  `npl_pharmaceutical_form_lx` varchar(32) DEFAULT NULL,
  `sv` varchar(128) DEFAULT NULL,
  `en` varchar(128) DEFAULT NULL
);
.separator \t
.import source/npl_pharmaceutical_form.csv npl_pharmaceutical_forms


-- Merge npl data into one table: se_medicines
--------------------------------------------------------------

DROP TABLE IF EXISTS `se`;

CREATE TABLE se AS
SELECT m.nplid, m.tradename  ,m.strength_group_name, f.sv AS pharmaceutical_form_group_name, o.organization
FROM npl_medicines m
JOIN npl_organizations o ON m.organization_lx = o.organization_lx
JOIN npl_pharmaceutical_form_groups f ON m.pharmaceutical_form_group_lx = f.pharmaceutical_form_group_lx;

UPDATE se SET tradename = replace(tradename, '®', '') WHERE tradename like '%®%';

DROP TABLE IF EXISTS `ndc_medicines`;

CREATE TABLE `ndc_medicines` (
  `PRODUCTID` ,
  `PRODUCTNDC`  ,
  `PRODUCTTYPENAME` ,
  `PROPRIETARYNAME` ,
  `PROPRIETARYNAMESUFFIX` ,
  `NONPROPRIETARYNAME`  ,
  `DOSAGEFORMNAME`  ,
  `ROUTENAME` ,
  `STARTMARKETINGDATE`  ,
  `ENDMARKETINGDATE`  ,
  `MARKETINGCATEGORYNAME` ,
  `APPLICATIONNUMBER` ,
  `LABELERNAME` ,
  `SUBSTANCENAME` ,
  `ACTIVE_NUMERATOR_STRENGTH` ,
  `ACTIVE_INGRED_UNIT`  ,
  `PHARM_CLASSES` ,
  `DEASCHEDULE`
);
.separator \t
.import source/product.txt ndc_medicines

-- we don't want to import the first line of the csv
DELETE FROM ndc_medicines WHERE `PRODUCTID` LIKE 'PRODUCTID';

DROP TABLE IF EXISTS `us`;

CREATE TABLE us AS 
select
PRODUCTNDC as nplid,
PROPRIETARYNAME as tradename,
CASE 
  WHEN ACTIVE_INGRED_UNIT NOT LIKE "%[%"
  THEN 
    CASE
      WHEN ACTIVE_NUMERATOR_STRENGTH LIKE "%;%" THEN ''
      ELSE ACTIVE_NUMERATOR_STRENGTH
    END  ||
  CASE 
    WHEN ACTIVE_INGRED_UNIT LIKE "%;%" THEN ''
    ELSE ACTIVE_INGRED_UNIT
  END 
END as strength_group_name, 
LOWER(SUBSTR(DOSAGEFORMNAME,  0,instr(DOSAGEFORMNAME,',') )) as pharmaceutical_form_group_name,
LABELERNAME as organization
from ndc_medicines
WHERE tradename NOT LIKE ''
AND MARKETINGCATEGORYNAME NOT LIKE 'OTC MONOGRAPH NOT FINAL'
AND MARKETINGCATEGORYNAME NOT LIKE 'OTC MONOGRAPH FINAL'
AND MARKETINGCATEGORYNAME NOT LIKE 'UNAPPROVED MEDICAL GAS'
AND MARKETINGCATEGORYNAME NOT LIKE 'UNAPPROVED HOMEOPATHIC'
AND MARKETINGCATEGORYNAME NOT LIKE 'UNAPPROVED DRUG OTHER'
GROUP BY tradename,strength_group_name,pharmaceutical_form_group_name,organization;

UPDATE us SET tradename = replace(tradename, '®', '') WHERE tradename like '%®%';

-- Delete unused tables
----------------------------------------------------------------

DROP TABLE IF EXISTS `npl_medicines`;

DROP TABLE IF EXISTS `npl_organizations`;

DROP TABLE IF EXISTS `npl_pharmaceutical_form_groups`;

DROP TABLE IF EXISTS `npl_pharmaceutical_forms`;

DROP TABLE IF EXISTS `ndc_medicines`;

COMMIT;

-- Vacuum the remaining tables
----------------------------------------------------------------
VACUUM se;
VACUUM us;