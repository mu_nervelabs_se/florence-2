<?xml version="1.0" encoding="UTF-8" ?>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <xsl:output method="text" />
        <xsl:template match="/">
<xsl:for-each select="xs:schema/xs:simpleType/xs:restriction">
                <xsl:for-each select="xs:enumeration">
                    <xsl:value-of select="@value" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:for-each select="xs:annotation/xs:documentation">
                        <xsl:value-of select="text()" />
                        <xsl:text>&#x9;</xsl:text>
                    </xsl:for-each>
                    <xsl:text>&#xa;</xsl:text>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:template>
    </xsl:stylesheet>