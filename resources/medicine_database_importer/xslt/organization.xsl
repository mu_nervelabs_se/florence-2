<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:npl="urn:schemas-npl:instance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:schemas-npl:instance ../schemas/npl-aux.xsd">
<xsl:output method="text" />
<xsl:template match="/">
<xsl:for-each select="npl:organizations/npl:organization">
            <xsl:value-of select="@id" />
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="npl:orgname" />
            <xsl:text>&#x9;</xsl:text>
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>