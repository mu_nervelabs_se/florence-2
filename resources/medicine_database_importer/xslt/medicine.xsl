<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:npl="urn:schemas-npl:instance" xmlns:apo="urn:schemas-npl:apoteket" xmlns:ind="urn:schemas-npl:industry" xmlns:lfn="urn:schemas-npl:lfn" xmlns:mpa="urn:schemas-npl:mpa" xmlns:util="urn:schemas-npl:util">
    <xsl:output method="text" />
    <xsl:template match="/">
<xsl:for-each select="npl:envelope/npl:medprod">
            <xsl:for-each select="npl:medprodpack/npl:presentation">
                <xsl:if test="not(../../npl:dates/mpa:date[@type='withdrawaldate']/@v)">    
                    <xsl:value-of select="../../npl:identifiers/mpa:identifier[@type='nplid']/@v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:value-of select="../../npl:names/ind:name[@type='tradename']/@v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:value-of select="../../ind:strength-text/ind:v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:value-of select="../../mpa:pharmaceutical-form-group-lx/@v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:value-of select="../../mpa:pharmaceutical-form-lx/@v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:value-of select="../../npl:organizations/mpa:organization/mpa:organization-lx/@v" />
                    <xsl:text>&#x9;</xsl:text>
                    <xsl:text>&#xa;</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>