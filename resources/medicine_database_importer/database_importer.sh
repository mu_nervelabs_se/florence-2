#!/bin/bash
#     
# SQLite Nerve Medicine database importer
#
# Import data from:
# 	NPL - Nationellt Produktregister för Läkemedel (SE): All information from NPL is exported to XML and XSD-files daily. Files are stored in zip-archive and available for download from NPL homesite.
#	NDC - National Drug Code (US): All information from NDC is exported to a zip-archive and available for download from FDA homesite. 
#
# Generate a SQLite and a SQL file located in the www folder of the app
# Abort if any command returns something else than 0

#
#pillbox info (not in production)
#curl -o source/pillbox.csv http://pillbox.nlm.nih.gov/downloads/pillbox_engine_20150511.tab
#sqlite3 client_db.sqlite < sql/pillbox_medicines_merge.sql
#

set -e
start=`date +%s`
echo ""
echo "---> SQLite NERVE MEDICINE DATABASE IMPORTER"
echo "     ᕦ(ò_óˇ)ᕤ"
echo ""

echo "---> Download npl.tgz"
curl -o source/npl.tgz https://npl.mpa.se/mpa.npl.services/publicering/npl.tgz

echo "---> Download ndc.zip"
curl -o source/ndc.zip http://www.accessdata.fda.gov/cder/ndc.zip

echo "---> Decompress npl files"
tar -xzf source/npl.tgz -C source

echo "---> Decompress ndc files"
unzip -o -q source/ndc.zip -d source

echo "---> Convert npl files XML to csv, it takes time..Shrinking 1GB to 1MB.. minimum 30 sec"
#we sort out duplicates ( sort -u )
xsltproc xslt/medicine.xsl source/NplProducts.xml  | sort -u > source/npl_medicine.csv
xsltproc xslt/organization.xsl source/organization.xml > source/npl_organization.csv
xsltproc xslt/pharmaceutical-form-group.xsl source/pharmaceutical-form-group-lx.xsd > source/npl_pharmaceutical_form_group.csv
xsltproc xslt/pharmaceutical-form.xsl source/pharmaceutical-form-lx.xsd > source/npl_pharmaceutical_form.csv

echo "---> Create the SQLite file from csv files"
sqlite3 ../www/nerve_medicines.db < sql/medicines_merge.sql
echo "NOTA: EXPECTED * COLUMNS BUT FOUND * - EXTRAS IGNORED ... IS NORMAL"

echo "---> Export the SQLite Database in SQL format"
sqlite3 ../www/nerve_medicines.db .dump > ../www/nerve_medicines.sql

end=`date +%s`
runtime=$((end-start))
echo ""
echo "---> Done in " $runtime "seconds!"
echo "ѧѦ ѧ ︵︵ ̢ ̱ ̧̱ι̵̱̊ι̶̨̱ ̶̱ ︵ Ѧѧ ︵︵ ѧ Ѧ ̵̗̊o̵̖ ︵ ѦѦ ѧ "