#!/bin/bash
#
# Creates a signed and zipaligned APK
#
# Don't forget to gitignore your key and your compiled apks.
#
# Original at https://gist.github.com/th3m4ri0/acc2003adc7dffdbbad6
# Author : Erwan d'Orgeville<info@erwandorgeville.com>

# Abort if any command returns something else than 0

set -e
start=`date +%s`
appname="Florence"
echo "---> FLORENCE ANDROID RELEASE"
echo "     ᕦ(ò_óˇ)ᕤ"
echo ""


echo "---> Generate a release build, based on the settings in your config.xml"
ionic build --release android


echo ""
echo "---> JAR signing"
jarsigner -sigalg SHA1withRSA -digestalg SHA1 -tsa http://timestamp.digicert.com -keystore my-release-key.keystore -storepass "/\? ]e{3)3u(~0T" platforms/android/build/outputs/apk/android-release-unsigned.apk alias_name


echo ""
echo "---> Zip aligning"
~/Library/Android/sdk/build-tools/25.0.2/zipalign -f 4 platforms/android/build/outputs/apk/android-release-unsigned.apk Florence.apk


echo ""
echo "---> Install on phone"
#'-r' means reinstall the app, keeping its data
~/Library/Android/sdk/platform-tools/adb install -r Florence.apk


end=`date +%s`
runtime=$((end-start))
afplay /System/Library/Sounds/Ping.aiff
afplay /System/Library/Sounds/Ping.aiff
afplay /System/Library/Sounds/Ping.aiff
echo ""
echo "---> Done in " $runtime "seconds!"
echo "ѧѦ ѧ ︵︵ ̢ ̱ ̧̱ι̵̱̊ι̶̨̱ ̶̱ ︵ Ѧѧ ︵︵ ѧ Ѧ ̵̗̊o̵̖ ︵ ѦѦ ѧ "
