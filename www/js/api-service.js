angular.module('florence.api', [])
    .factory('apiService', ['$http', '$rootScope', function($http, $rootScope) {
        //var baseURL = 'http://localhost:8080/nervelabsRest';
        var baseURL = 'https://api.nervelabs.se';

        return {
            misfit: {
                getSessions: function(summarySearch) {
                    return $http.get(baseURL + '/rest/misfitSessions.json', {
                        params: summarySearch
                    });
                },
                getSleep: function(summarySearch) {
                    return $http.get(baseURL + '/rest/misfitSleeps.json', {
                        params: summarySearch
                    });
                },
                getSummary: function(summarySearch) {
                    return $http.get(baseURL + '/rest/misfitSummary.json', {
                        params: summarySearch
                    });
                },
                getDevice: function() {
                    return $http.get(baseURL + '/rest/misfitDevice.json');
                },
                getProfile: function() {
                    return $http.get(baseURL + '/rest/misfitProfile.json');
                },
                getAccess: function() {
                    return $http.get(baseURL + '/rest/misfitAuthorizeURL.json');
                },
                disconnect: function() {
                    return $http.get(baseURL + '/rest/misfitDisconnect.json');
                }
            },

            class: {
                getClasses: function(tags) {
                        console.log("tags-----------------------");
                        //console.log(tags);
                        return $http.post(baseURL + '/rest/listClasses.json', _.map(tags, 'id'), {
                            params: {
                                language: $rootScope.classLanguageCode
                            }
                        });
                    },
                    show: function(id) {
                        return $http.get(baseURL + '/rest/showClass.json', {
                            params: {
                                id: id
                            }
                        });
                    },
                    availableTags: function() {
                        return $http.get(baseURL + '/rest/listTags.json');
                    }
            },

            user: {
                current: function() {
                    return $http.get(baseURL + '/rest/currentUser.json');
                },
                register: function(data) {
                    return $http.post(baseURL + '/auth/register.json', data);
                },
                login: function(data) {
                    return $http.post(baseURL + '/auth/login.json', data);
                },
                logout: function() {
                    return $http.get(baseURL + '/j_spring_security_logout');
                },
                logout2: function() {
                    return $http.get(baseURL + '/auth/logout.json');
                },
                sendPasswordResetMail: function(email) {
                    return $http.get(baseURL + '/auth/sendPasswordResetMail.json', {
                        params: {
                            email: email
                        }
                    });
                },

                changePassword: function(data) {
                    return $http.post(baseURL + '/auth/changePassword.json', data);
                },

                changeEmailAddress: function(params) {
                    return $http.get(baseURL + '/auth/changeEmailAddress.json', {
                        params: params
                    });
                }
            }
        };
    }]);

angular.module('florence.api')
    .factory('httpInterceptor', ['$q', 'localStorageService', function($q, localStorageService) {
        return {
            request: function(config) {
                if (config.url.indexOf('api.nervelabs.se') >= 0 || config.url.indexOf('localhost:8080') >= 0) {
                    config.params = config.params || {};
                    if (localStorageService.get('user')) {
                        config.params['Florence-Token'] = localStorageService.get('user').id;
                        //console.log('fetched with token', angular.toJson(config, true));
                    }

                }
                return config || $q.when(config);
            },
            response: function(response) {
                return response || $q.when(response);
            },
            responseError: function(response) {
                return $q.reject(response);
            }
        };
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.interceptors.push('httpInterceptor');
    }]);
