angular.module('florence.directives')

    .directive("flip", function($rootScope, $timeout) {
        return {
            restrict: "A",
            scope: true,
            link: function($scope, element) {

                $timeout(function() {
                    var $panels = element.css({
                        position: 'relative',
                        margin: 'auto'
                    }).addClass('flip').children().addClass("flip-panel");

                    var frontPanel = $panels.eq(0);
                    var backPanel = $panels.eq(1);

                    $scope.showFrontPanel = function() {
                        frontPanel.removeClass("flip-hide-front-panel");
                        backPanel.addClass("flip-hide-back-panel");
                    };

                    $scope.showBackPanel = function(page) {
                        backPanel.removeClass("flip-hide-back-panel");
                        frontPanel.addClass("flip-hide-front-panel");
                        $scope.template = page;
                    };

                    $scope.deleteItem = function() {
                        alert('deleted');
                    };

                    $scope.showFrontPanel();


                    $scope.$on('showBackPanel', function(e, data) {
                        $scope.showBackPanel(data);
                    });
                    $scope.$on('showFrontPanel', function(e, data) {
                        $scope.showFrontPanel();
                    });
                });
            }
        };
    });
