angular.module('florence.translate', ['pascalprecht.translate'])

  .config(function($translateProvider) {

    $translateProvider.fallbackLanguage('en');

    $translateProvider.translations('en', {
      "FLORENCE_GREETING": "Welcome to Florence.",
      "FLORENCE_BASELINE": "A place where you can keep track of your daily life and improve your health.",
      "REGISTER": "Register",
      "CREATE_NEW_ACCOUNT": "Create a new account",
      "LOGIN": "Login",
      "POSTPONE": "Postpone",
      "POSTPONE_SAVE": "Save & Postpone",
      "POSTPONE_ALL": "Postpone All",
      "POSTPONE_TIME": "New Time",
      "PASSWORD": "Password",
      "PASSWORD-CURRENT": "Current password",
      "PASSWORD-NEW": "New password",
      "PASSWORD-REPEAT": "Repeat password",
      "FORGOT_PASSWORD": "Forgot password?",
      "RESET_PASSWORD": "Reset password",
      "CHANGE_PASSWORD": "Change password",
      "CHANGE_EMAIL": "Change email",
      "CURRENT_EMAIL": "Current email",
      "NEW_EMAIL": "New email",
      "FORGOT_PASSWORD_SENT": "An Email was sent to you with further instructions.",
      "CHANGE_PASSWORD_SUCCESS": "Your password has successfully been changed. For security reasons, you will now be logged out.",
      "PASSWORD_RESET_CONFIRM": "Are you sure you want to reset your password? You will be logged out.",
      "PASSWORD_DOESNT_MATCH_OLD": "The old password does not match with your current password. Please try again.",
      "PASSWORDS_DONT_MATCH": "Your new passwords do not match. Please try again.",
      "PASSWORD_LENGTH_INVALID": "Your new passwords needs to contain at least 6 characters.",
      "PASSWORD_MISSING_DATA": "You have some missing Data. Please check the form again and re-submit.",
      "EMAIL_REQUIRED": "You have to enter an email.",
      "EMAIL_IN_USE": "The email you entered is already in use.",
      "EMAIL_FORMAT_INVALID": "The email address you entered is invalid.",
      "PRIVACY_TERMS": "Privacy Terms",
      "REMINDERS": "Reminders",
      "REMINDER_TIME": "Reminder Time",
      "DELETE_CARD": "Delete card",
      "DELETE_CARD_CONFIRM": "Are you sure you want to delete this card?",
      "CLASSES": "Classes",
      "CHARTS": "Charts",
      "DATA": "Data",
      "SYSTEM": "System",
      "SLEEP": "Sleep",
      "FOOD": "Food",
      "MOOD": "Mood",
      "EXERCISE": "Exercise",
      "PROFILE": "Profile",
      "SETTINGS": "Settings",
      "REMINDER": "Reminder",
      "ADDTOLIST": "Add to list",
      "ADDANOTHER": "Add another",
      "MEDICATION_LIST": "Medication List",
      "DEBUG": "Debug",
      "FEEDBACK": "Support",
      "SEND_EMAIL": "Send e-mail",
      "SEND": "Send",
      "DIARY": "Diary",
      "TODAY": "Today",
      "YESTERDAY": "Yesterday",
      "PLUSCARD": "Plus Card",
      "ADD_MEDICATION": "Add medication",
      "SEARCH_MEDICATION": "Search medication",
      "SEARCHING": "Searching",
      "NO_RESULTS_FOUND": "No results found",
      "WHAT_DOSAGE": "What dosage?",
      "DOSAGE": "Dosage",
      "TIME_TAKEN": "Time taken",
      "DATE_TAKEN": "Date taken",
      "TAKEN": "Taken",
      "ON_TIME": "On time",
      "TAKEN_LATER": "Taken later",
      "NOT_TAKEN": "Not Taken",
      "TIME_FREQUENCY": "Time & Frequency",
      "DAYS_OF_THE_WEEK": "Days of the Week",
      "SLEEP_MOTION_CHART": "Sleep/Motion Chart",
      "AVERAGE": "Average",
      "MEDICATION_REMINDER": "Medication Reminder",
      "MEDICATION_REMINDER_GREETING": "What medication did you take?",
      "MEDICATION_REMINDER_POSTPONE": "Postpone Reminder",
      "MEDICATION_DATABASE_CHANGE": "Change medication database",
      "MEDICATION_DATABASE_CHANGE_TEXT": "Select the medication database that you would like to use. This is used in the reminder and Plus Cards.",
      "USERNAME": "Username",
      "FIRST_NAME": "First Name",
      "LAST_NAME": "Last Name",
      "NAME": "Name",
      "EDIT_NAME": "Here you can update your name.",
      "EMAIL": "Email",
      "LOGOUT": "Logout",
      "LOGOUT_CONFIRMATION": "Are you sure you want to leave?",
      "ADD_IMAGE": "Add Image",
      "EDIT_PROFILE": "Edit Profile",
      "DATE_OF_BIRTH": "Date of birth",
      "HEIGHT": "Height",
      "WEIGHT": "Weight",
      "WOMAN": "Woman",
      "MAN": "Man",
      "TESTS": "Tests",
      "TAPTEST": "Tap test",
      "TAPS": "taps",
      "FINGER_TAP": "Fingertap",
      "TAP_TO_START": "Tap to start",
      "KEEP_GOING": "Keep going!",
      "GOOD_JOB": "Good Job!",
      "GO": "Go!",
      "SAVEEXIT": "All done! Save to exit.",
      "ADD_DATA": "Add Data Source",
      "NO_DATA": "No Data",
      "MEDICATION": "Medication",
      "MEDICINE": "Medicine",
      "ACTIVITY": "Activity",
      "LEFT_HAND": "Left Hand",
      "RIGHT_HAND": "Right Hand",
      "CANCEL": "Cancel",
      "SAVE": "Save",
      "YES": "Yes",
      "NO": "No",
      "DELETE": "Delete",
      "RESET": "Reset",
      "DISCONNECT": "Disconnect",
      "DISCONNECT_DATA": "Disconnect data",
      "DISCONNECT_DATA_CONFIRM": "Are you sure you want to disconnect this data source?",
      "ADD TO LIST": "Add to list",
      "SELECT HAND": "Select hand",
      "NOWLEFTHAND": "Now your left hand",
      "NOWRIGHTHAND": "Now your right hand",
      "HTTP_0": "No Internet",
      "HTTP_200": "OK",
      "HTTP_401": "Unauthorized",
      "HTTP_406": "User Credential Error",
      "HTTP_500": "Internal Server Error",
      "HTTP_503": "User Login Error",
      "ADD_TAKEN_MEDICATION": "Add Taken Medication",
      "MEDICATION_ALREADY_TAKEN": "This medication has already been taken",
      "DO_YOU_WANT_TO_TAKE_MEDICATION_AGAIN": "Are you sure you want to take it again?",
      "LOADING": "Loading",
      "MISFIT_CONNECTION_ERROR": "There was a problem getting your sleep data",
      "WEEK": "Week",
      "INTERESTS": "Interests",
      "EXPORT": "Export",
      "NOTE": "Note",
      "WASSAP?": "Take a note. Max 400 characters.",
      "THANK_YOU_WELCOME_TITLE": "THANK YOU & WELCOME",
      "THANK_YOU_WELCOME_TEXT": "We appreciate you taking the time to download Florence and hope it will support you in a good way. If you like Florence please give it a good review in App Store to make it easier for others to find Florence.",
      "WHAT_DO_YOU_THINK_TITLE": "WHAT DO YOU THINK",
      "WHAT_DO_YOU_THINK_TEXT_1": "We want to get your opinion on what works and what can be improved.",
      "WHAT_DO_YOU_THINK_TEXT_2": " an e-mail with tips, bugs or questions.",
      "WHO_CREATED_FLORENCE_TITLE": "WHO CREATED FLORENCE",
      "WHO_CREATED_FLORENCE_TEXT": "Florence is created by a small team with personal experience of chronic conditions and medication management. Florence is published by Nerve that aims to bring smart technology to support better health for everyone.",
      "MORE_INFO_TEXT": "More info about us is found here:",
      "MADE_IN_TEXT": "Made with ❤ in Stockholm, Sweden",
      "CONTACT": "Contact",
      "TOS": "By creating an account you agree to the",
      "TERMS": "terms",
      "TERMS_URL": "http://en.florence.cards/terms/",
      "AND": "and",
      "PRIVACY_POLICY": "Privacy Policy",
      "PRIVACY_POLICY_URL": "http://en.florence.cards/privacy_policy/",
      "VERSION": "Version",
      "BUILD": "Build",
      "PLUSONE_MOOD_TITLE": "Mood",
      "HOW_IS_YOUR_MOOD?": "How is your mood right now?",
      "HOW_WAS_YOUR_MOOD?": "How was your mood?",
      "PLUSONE_HELP_MOOD": "Use the Mood Plus Card to save how you feel right now. The scale goes from one (1) which is bad and five (5), which is good.",
      "PLUSONE_HELP_NOTE": "Use this Plus Card to make a note of just about anything, you decide.",
      "PLUSONE_HELP_MEDICINE_INTAKE_ADD": "Select dosage and save the intake in the Diary.",
      "PLUSONE_HELP_MEDICINE_CHOOSE": "Here you create a medication Plus Card that makes it fast and easy to save an intake to the Diary.",
      "PROFILE_HELP_CHANGE_PASSWORD": "It is easy to change your password. Enter the passwords and save and it's done.",
      "PROFILE_HELP_CHANGE_EMAIL": "You can easily change your e-mail by enter the new email and save.",
      "PROFILE_SELECT_TAGS": "We have dynamic content in Florence that might be intersting to you. Select the tag that interest you and this content will be visible when it is available. The content is language specific and might differ depending on your language settings.",
      "PAIN_TYPE": "Type of pain",
      "PAIN": "Pain",
      "PLUSONE_PAIN_TITLE": "Pain",
      "HOW_IS_YOUR_PAIN": "What type & how much pain are you in now?",
      "PLUSONE_HELP_PAIN": "Use the Plus card to keep track of your pain level. The scale is from zero (0) which is no pain to to ten (10) which is unbearable. A higher number is more pain.",
      "HOW_WAS_YOUR_PAIN": "How much pain did you have?",
      "NO_PAIN": "No pain",
      "VERY_MILD": "Very mild",
      "DISCOMFORTING": "Discomforting",
      "TOLERABLE": "Tolerable",
      "DISTRESSING": "Distressing",
      "VERY_DISTRESSING": "Very distressing",
      "INTENSE": "Intense",
      "VERY_INTENSE": "Very intense",
      "UTTERLY_HORRIBLE": "Utterly horrible",
      "EXCRUCIATING_UNBEARABLE": "Excruciating unbearable",
      "UNIMAGINABLE_UNSPEAKABLE": "Unimaginable unspeakable",
      "DULL": "Dull",
      "BURNING": "Burning",
      "SCURRYING": "Scurrying",
      "INCISIVE": "Incisive",
      "PULSATING": "Pulsating",
      "PUNGENT": "Pungent",
      "SWELTERING": "Sweltering",
      "OTHER_PAIN": "other pain",
      "SE_NPL_DB": "Sweden",
      "US_NDC_DB": "US",
      "MEDICINE_DB": "Medicine database",
      "SELECT_DOSAGE_AND_SAVE_THE_INTAKE_IN_THE_DIARY": "Select dosage and save the intake in the Diary.",
      "LANGUAGE": "Language",
      "MONDAY": "monday",
      "TUESDAY": "tuesday",
      "WEDNESDAY": "wednesday",
      "THURSDAY": "thursday",
      "FRIDAY": "friday",
      "SATURDAY": "saturday",
      "SUNDAY": "sunday",
    });

    $translateProvider.translations('sv', {
      "FLORENCE_GREETING": "Välkommen till Florence.",
      "FLORENCE_BASELINE": "En plats där du kan hålla koll på ditt dagliga liv och förbättra din hälsa.",
      "REGISTER": "Registrera",
      "CREATE_NEW_ACCOUNT": "Skapa konto",
      "LOGIN": "Logga in",
      "POSTPONE": "Skjut upp",
      "POSTPONE_SAVE": "Spara & skjut upp",
      "POSTPONE_ALL": "Skjuta upp alla",
      "POSTPONE_TIME": "Ny tid",
      "PASSWORD": "Lösenord",
      "PASSWORD-CURRENT": "Nuvarande lösenord",
      "PASSWORD-NEW": "Nytt lösenord",
      "PASSWORD-REPEAT": "Bekräfta lösenord",
      "FORGOT_PASSWORD": "Glömt lösenord?",
      "RESET_PASSWORD": "Återställ lösenord",
      "CHANGE_PASSWORD": "Ändra lösenord",
      "CHANGE_EMAIL": "Ändra e-post",
      "CURRENT_EMAIL": "Nuvarande e-post",
      "NEW_EMAIL": "Ny e-postadress",
      "FORGOT_PASSWORD_SENT": "Ett e-post meddelande med instruktioner har skickats till dig.",
      "CHANGE_PASSWORD_SUCCESS": "Ditt lösenord har ändrats. För säkerhets skull kommer du att loggas ut.",
      "PASSWORD_RESET_CONFIRM": "Är du säker på att du vill återställa ditt lösenord? Du kommer att loggas ut.",
      "PASSWORD_DOESNT_MATCH_OLD": "Det gamla lösenordet stämmer inte. Prova igen.",
      "PASSWORDS_DONT_MATCH": "Ditt nya lösenord matchar inte. Prova igen.",
      "PASSWORD_LENGTH_INVALID": "Ditt nya lösenord måste innehålla minst 6 tecken.",
      "PASSWORD_MISSING_DATA": "Du har inte fyllt i allting. Kolla och skicka igen.",
      "EMAIL_REQUIRED": "Du måste fylla i en e-postadress.",
      "EMAIL_IN_USE": "Den e-postadress du använde finns redan.",
      "EMAIL_FORMAT_INVALID": "E-postadressen är ogiltig.",
      "PRIVACY_TERMS": "Sekretessvillkor",
      "REMINDERS": "Påminnelser",
      "REMINDER_TIME": "Påminnelsetid",
      "DELETE_CARD": "Ta bort kort",
      "DELETE_CARD_CONFIRM": "Är du säker på att du vill ta bort kortet?",
      "CLASSES": "Klasser",
      "CHARTS": "Diagram",
      "DATA": "Data",
      "SYSTEM": "Inställningar",
      "SLEEP": "Sömn",
      "FOOD": "Mat",
      "MOOD": "Humör",
      "EXERCISE": "Motion",
      "PROFILE": "Profil",
      "SETTINGS": "Inställningar",
      "DEBUG": "Debug",
      "FEEDBACK": "Support",
      "SEND_EMAIL": "Skicka e-post",
      "SEND": "Skicka",
      "DIARY": "Dagbok",
      "TODAY": "Idag",
      "YESTERDAY": "Igår",
      "REMINDER": "Påminnelse",
      "ADDTOLIST": "Lägg till listan",
      "ADDANOTHER": "Lägg till fler",
      "MEDICATION_LIST": "Läkemedelslista",
      "PLUSCARD": "Pluskort",
      "ADD_MEDICATION": "Lägg till läkemedel",
      "SEARCH_MEDICATION": "Sök läkemedel",
      "SEARCHING": "Söker...",
      "NO_RESULTS_FOUND": "Inga resultat",
      "WHAT_DOSAGE": "Vilken dos",
      "DOSAGE": "Dos",
      "TIME_TAKEN": "Tagit tid",
      "DATE_TAKEN": "Tagit datum",
      "TAKEN": "Tagit",
      "ON_TIME": "Tagit i tid",
      "TAKEN_LATER": "Tagit senare",
      "NOT_TAKEN": "Inte tagit",
      "TIME_FREQUENCY": "Tid & Frekvens",
      "DAYS_OF_THE_WEEK": "Veckodagar",
      "SLEEP_MOTION_CHART": "Sömn/Aktivitet diagram",
      "AVERAGE": "Medelvärde",
      "MEDICATION_REMINDER": "Läkemedelspåminnelse",
      "MEDICATION_REMINDER_POSTPONE": "Skjuta upp påminnelsen",
      "MEDICATION_REMINDER_GREETING": "Vilka läkemedel tog du?",
      "USERNAME": "Användarnamn",
      "FIRST_NAME": "Förnamn",
      "LAST_NAME": "Efternamn",
      "NAME": "Namn",
      "EDIT_NAME": "Här kan du ändra ditt namn.",
      "EMAIL": "E-post",
      "LOGOUT": "Logga ut",
      "LOGOUT_CONFIRMATION": "Är du säker på att du vill logga ut?",
      "ADD_IMAGE": "Lägg till bild",
      "EDIT_PROFILE": "Ändra profil",
      "DATE_OF_BIRTH": "Födelsedatum",
      "HEIGHT": "Längd",
      "WEIGHT": "Vikt",
      "WOMAN": "Kvinna",
      "MAN": "Man",
      "TAPS": "tappningar",
      "TESTS": "Tester",
      "TAPTEST": "Tap test",
      "FINGER_TAP": "Fingertap",
      "TAP_TO_START": "Klicka för att börja",
      "KEEP_GOING": "Fortsätt!",
      "GOOD_JOB": "Bra Jobbat!",
      "GO": "Börja!",
      "SAVEEXIT": "Klart! Spara för att avsluta.",
      "ADD_DATA": "Lägg till tjänst",
      "NO_DATA": "Ingen data",
      "MEDICATION": "Läkemedel",
      "MEDICINE": "Medicin",
      "ACTIVITY": "Aktivitet",
      "LEFT_HAND": "Vänster hand",
      "RIGHT_HAND": "Höger hand",
      "CANCEL": "Avbryt",
      "SAVE": "Spara",
      "YES": "Ja",
      "NO": "Nej",
      "DELETE": "Ta bort",
      "DISCONNECT": "Koppla bort",
      "DISCONNECT_DATA": "Koppla bort data",
      "DISCONNECT_DATA_CONFIRM": "Vill du verkligen koppla bort datakällan?",
      "RESET": "Återställ",
      "SELECT HAND": "Välj hand",
      "NOWLEFTHAND": "Använd vänster hand",
      "NOWRIGHTHAND": "Använd höger hand",
      "ADD TO LIST": "Lägg till",
      "HTTP_0": "Ingen uppkoppling",
      "HTTP_200": "OK",
      "HTTP_401": "Ej tillåtet",
      "HTTP_406": "Fel användarnamn eller lösenord",
      "HTTP_500": "Internt server-fel",
      "HTTP_503": "Fel vid inloggning",
      "ADD_TAKEN_MEDICATION": "Lägg till medicin",
      "MEDICATION_ALREADY_TAKEN": "Du har redan tagit den här medicinen",
      "DO_YOU_WANT_TO_TAKE_MEDICATION_AGAIN": "Vill du ta den igen?",
      "LOADING": "Loading",
      "MISFIT_CONNECTION_ERROR": "Problem att få din sömn data",
      "WEEK": "Vecka",
      "INTERESTS": "Intresseområden",
      "EXPORT": "Export",
      "NOTE": "Notera",
      "WASSAP?": "Skriv en notering. Max 400 tecken.",
      "THANK_YOU_WELCOME_TITLE": "TACK & VÄLKOMMEN",
      "THANK_YOU_WELCOME_TEXT": "Vi uppskattar att du har tagit dig tid att ladda ner Florence och hoppas appen är ett bra stöd. Om du tycker om Florence uppskattar vi om du ger den ett bra betyg på App Store. då det blir lättare för fler att hitta Florence.",
      "WHAT_DO_YOU_THINK_TITLE": "VAD TYCKER DU",
      "WHAT_DO_YOU_THINK_TEXT_1": "Vi vill höra vad du tycker fungerar bra och vad som kan förbättras.",
      "WHAT_DO_YOU_THINK_TEXT_2": "ett mail direkt till oss med förslag, buggar eller frågor.",
      "WHO_CREATED_FLORENCE_TITLE": "VEM HAR SKAPAT FLORENCE",
      "WHO_CREATED_FLORENCE_TEXT": "Vi som har skapat Florence är ett litet team med personlig erfarenhet kring kronisk sjukdom och medicinering. Florence publiceras av Nerve som genom smart teknik vill stödja bättre hälsa för alla.",
      "MORE_INFO_TEXT": "Mer info om oss hittar du här:",
      "MADE_IN_TEXT": "Made with ❤ in Stockholm, Sweden",
      "CONTACT": "Kontakt",
      "TOS": "När du skapar ett konto accepterar du våra",
      "TERMS": "villkor",
      "TERMS_URL": "http://sv.florence.cards/villkor/",
      "AND": "och",
      "PRIVACY_POLICY": "sekretesspolicy",
      "PRIVACY_POLICY_URL": "http://sv.florence.cards/sekretess_policy/",
      "VERSION": "Version",
      "BUILD": "Build",
      "PLUSONE_MOOD_TITLE": "Humör",
      "HOW_IS_YOUR_MOOD?": "Hur känner du dig nu?",
      "HOW_WAS_YOUR_MOOD?": "Hur kände du dig?",
      "PLUSONE_HELP_MOOD": "Använd humör Plus kortet till att spara hur du mår just nu. Skalan går från ett (1) som är dåligt och fem (5) som är bra.",
      "PLUSONE_HELP_NOTE": "Använd detta Plus kort till att göra en anteckning om precis vad som helst, du bestämmer.",
      "PLUSONE_HELP_MEDICINE_INTAKE_ADD": "Markera dos och spara intaget i Dagboken.",
      "PLUSONE_HELP_MEDICINE_CHOOSE": "Här skapar du ett Plus kort för mediciner som gör det enkelt och snabbt att spara ett intag i Dagboken.",
      "PROFILE_HELP_CHANGE_PASSWORD": "Det är enkelt att byta ditt lösenord. Fyll i lösenorden och spara så är det klart.",
      "PROFILE_HELP_CHANGE_EMAIL": "Du kan enkelt byta din e-post genom att fylla i den nya e-posten och spara.",
      "PROFILE_SELECT_TAGS": "I Florence finns dynamiskt innehåll som kanske intresserar dig. Välj den eller de taggar som intresserar dig så blir det synligt när det är tillgängligt.",
      "PAIN_TYPE": "Typ av smärta",
      "PAIN": "Smärta",
      "PLUSONE_PAIN_TITLE": "Smärta",
      "HOW_IS_YOUR_PAIN": "Vilken typ & hur mycket smärta har du nu?",
      "PLUSONE_HELP_PAIN": "Använd Pluskortet för att hålla reda på din smärtnivå. Skalan är från noll (0) vilket betyder ingen smärta till tio (10) som betyder outhärdligt. Ett högre nummer betyder mer smärta.",
      "HOW_WAS_YOUR_PAIN": "Hur mycket smärta hade du?",
      "DULL": "Molande",
      "BURNING": "Brännande",
      "SCURRYING": "Ilande",
      "INCISIVE": "Skärande",
      "PULSATING": "Pulserande",
      "PUNGENT": "Stickande",
      "SWELTERING": "Tryckande",
      "OTHER_PAIN": "Övrig smärta",
      "SE_NPL_DB": "Svensk",
      "US_NDC_DB": "USA",
      "MEDICINE_DB": "Läkemedelsdatabas",
      "LANGUAGE": "Språk",
      "MONDAY": "måndag",
      "TUESDAY": "tisdag",
      "WEDNESDAY": "onsdag",
      "THURSDAY": "torsdag",
      "FRIDAY": "fredag",
      "SATURDAY": "lördag",
      "SUNDAY": "söndag",
    });
  })
