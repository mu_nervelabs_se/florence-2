angular.module('florence.controllers')

    .controller('ServiceCtrl', [
        '$scope', 'apiService', '$cordovaInAppBrowser', '$rootScope', 'User', '$stateParams', '$timeout', '$state', '$ionicPopup', '$filter',
        function($scope, apiService, $cordovaInAppBrowser, $rootScope, User, $stateParams, $timeout, $state, $ionicPopup, $filter) {

            User.get().then(function(user) {
                $scope.user = user;
            });

            $timeout(function() {
                if ($stateParams.service) {
                    $scope.service = $stateParams.service;
                    $rootScope.$broadcast('showBackPanel', $stateParams.service);
                }
            });

            $scope.openBrowser = function(url) {
                if (url) {
                    $cordovaInAppBrowser.open(url, '_blank')
                        .then(function(event) {
                            // success
                        })
                        .catch(function(event) {
                            // error
                        });
                } else {
                    alert('already connected to misfit');
                }
            };

            $scope.connectService = function(service) {
                switch (service) {
                    case 'misfit':
                        apiService.misfit.getAccess().success(function(data, status, headers, config) {
                            //console.log(angular.toJson(config, true));
                            $scope.openBrowser(data.authorizeUrl);
                        }).
                        error(function(data, status, headers, config) {
                            //console.log(angular.toJson(config, true));
                        });
                }
            };

            $scope.disconnect = function(service) {
                switch (service) {
                    case 'misfit':
                        apiService.misfit.disconnect().success(function(data) {
                            User.updateUser({
                                misfitToken: null
                            });
                            $state.go('tab.deck');
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                        });
                }
            };

            $scope.disconnectConfirmPopUp = function(service) {
                var header = $filter('translate')('DISCONNECT_DATA');
                var body = $filter('translate')('DISCONNECT_DATA_CONFIRM');
                var button1 = $filter('translate')('CANCEL');
                var button2 = $filter('translate')('DISCONNECT');

                var confirmService = service;
                disconnectPopup = $ionicPopup.show({
                    title: header,
                    template: body,
                    scope: $scope,
                    buttons: [{
                        type: 'button button-clear button-assertive',
                        text: button1,
                        onTap: function(e) {
                            console.log('service not disconnected');
                            disconnectPopup.close();
                        }
                    }, {
                        type: 'button button-clear button-balanced',
                        text: button2,
                        onTap: function(e) {
                            $scope.disconnect(confirmService);
                            console.log('service disconnected');
                            disconnectPopup.close();
                        }
                    }]
                });
                disconnectPopup.then(function(res) {
                    console.log('done');
                });
            };

            $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                apiService.user.current().success(function(data) {
                    User.updateUser({
                        misfitToken: data.misfitToken
                    });
                });

            });
        }
    ]);
