angular.module('florence.controllers')

    .controller('TaptestChartCtrl', function($scope, apiService, fingerTap, $state, $ionicSlideBoxDelegate, $timeout, $ionicPopup, $filter) {

        console.log('TaptestChartCtrl START', moment().format('MMMM Do YYYY, h:mm:ss a'));
        //Time range: last 7 days.
        var start_date = moment().subtract(6, 'days').format('YYYY-MM-DD');
        var end_date = moment().subtract(0, 'days').format('YYYY-MM-DD');

        //We prefilled with days
        $scope.charts = {};
        for (var i = 6; i >= 0; i--) {
            var day = moment().subtract(i, 'days').format('YYYY-MM-DD');
            $scope.charts[day] = {};
        }

        //we force Xaxis to min/max for line charts
        var forceXmin = 0 - (moment(0).utcOffset() * 60000);
        var forceXmax = 86399999 - (moment(0).utcOffset() * 60000);

        var forceYmin = 80;
        var forceYmax = 0;

        var noDataText = $filter('translate')('NO_DATA');

        // Debug filter with state, background load with refresh option should be better.


        /**
         * [getFingerTaps description]
         * @return {[type]}
         */

        $scope.getFingerTaps = function() {


            fingerTap.getAll().then(function(response) {
                    var fingerTapByDay = _.groupBy(response, function(it) {
                        return moment(it.doc.timestamp).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                    });
                    //alert(JSON.stringify(response.length));
                    var allTaps = response;
                    var averageRight = [];
                    var averageLeft = [];
                    var leftCount = 0;
                    var rightCount = 0;
                    $scope.highLeft = 0;
                    $scope.highRight = 0;
                    $scope.lowLeft = 100;
                    $scope.lowRight = 100;


                    _.forEach($scope.charts, function(data, day) {

                        $scope.charts[day].fingerTapChart = {};
                        $scope.charts[day].date = moment(day).format('LL');


                        if (fingerTapByDay[day]) {

                            fingerTapByDay[day] = _.sortBy(fingerTapByDay[day], 'doc.timestamp');
                            var leftHand = [];
                            var rightHand = [];


                            _.forEach(fingerTapByDay[day], function(data, session) {
                                if (data.doc.hand == 'l') {
                                    var relativeSessionStart = moment(data.doc.timestamp).diff(moment(day)) - (moment(0).utcOffset() * 60000);
                                    leftHand.push({
                                        x: relativeSessionStart,
                                        y: data.doc.taps
                                    });
                                    averageLeft.push({
                                        y: data.doc.taps,
                                        x: i
                                    });
                                }
                            });

                            _.forEach(fingerTapByDay[day], function(data, session) {
                                if (data.doc.hand == 'r') {
                                    var relativeSessionStart = moment(data.doc.timestamp).diff(moment(day)) - (moment(0).utcOffset() * 60000);
                                    rightHand.push({
                                        x: relativeSessionStart,
                                        y: data.doc.taps
                                    });
                                    averageRight.push({
                                        y: data.doc.taps,
                                        x: i
                                    });
                                }
                            });

                            for (var i = 0, iL = rightHand.length; i < iL; i++) {
                                if (rightHand[i] && leftHand[i]) {
                                    leftHand[i].x = rightHand[i].x;
                                }
                            }

                            for (var i = 0, iL = averageLeft.length; i < iL; i++) {
                                averageLeft[i].x = i;
                                if (averageLeft[i].y > $scope.highLeft) {
                                    $scope.highLeft = averageLeft[i].y
                                };
                                if (averageLeft[i].y < $scope.lowLeft) {
                                    $scope.lowLeft = averageLeft[i].y
                                };
                            };

                            for (var i = 0, iL = averageRight.length; i < iL; i++) {
                                averageRight[i].x = i;
                                if (averageRight[i].y > $scope.highRight) {
                                    $scope.highRight = averageRight[i].y
                                };
                                if (averageRight[i].y < $scope.lowRight) {
                                    $scope.lowRight = averageRight[i].y
                                };
                            };

                            var RHLabel = $filter('translate')('RIGHT_HAND');
                            var LHLabel = $filter('translate')('LEFT_HAND');

                            var averageData = [{
                                values: averageLeft, //values - represents the array of {x,y} data points
                                key: LHLabel, //key  - the name of the series.
                                color: '#FF9800' //color - optional: choose your own line color.
                            }, {
                                values: averageRight, //values - represents the array of {x,y} data points
                                key: RHLabel, //key  - the name of the series.
                                color: '#9C27B0' //color - optional: choose your own line color.
                            }];

                            $scope.averageTaps = averageData;
                            //alert(JSON.stringify(averageData));


                            data = [{
                                values: leftHand, //values - represents the array of {x,y} data points
                                key: LHLabel, //key  - the name of the series.
                                color: '#FF9800',
                                disabled: false, //color - optional: choose your own line color.
                            }, {
                                values: rightHand, //values - represents the array of {x,y} data points
                                key: RHLabel, //key  - the name of the series.
                                color: '#9C27B0',
                                disabled: false, //color - optional: choose your own line color.
                            }];


                            $scope.charts[day].fingerTapChart.data = data;
                            data = [];
                        } else {
                            $scope.charts[day].fingerTapChart.data = [];
                        }

                    });
                    for (var i = 0, iL = averageLeft.length; i < iL; i++) {
                        leftCount = leftCount + averageLeft[i].y
                    };
                    for (var i = 0, iL = averageRight.length; i < iL; i++) {
                        rightCount = rightCount + averageRight[i].y
                    };

                    $scope.leftCountAverage = Math.round(leftCount / averageLeft.length);
                    $scope.rightCountAverage = Math.round(rightCount / averageRight.length);
                    $scope.slideBoxUpdate('fingerTap');
                },
                function(err) {
                    console.log(err);
                });
        }

        $scope.getFingerTaps();


        $scope.averageTapChartOptions = {
            chart: {
                type: 'lineChart',
                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + moment(graph.point.x).format('HH:mm') + '</h3>' + y + '  taps';
                },
                interpolate: 'basis',
                height: 170,
                margin: {
                    top: 20,
                    right: 30,
                    bottom: 20,
                    left: 30
                },
                legend: {
                    width: 100,
                    height: 30,
                    align: true,
                    rightAlign: false
                },
                valueFormat: d3.format(',f'),
                tickFormat: d3.format(',f'),
                duration: 500,
                reduceXTicks: false,
                groupSpacing: 0.5,
                staggerLabels: false,
                forceY: [80, 10],
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    axisLabel: 'Taps'
                },
                showControls: false,
                showXAxis: false,
                showYAxis: true,
                showValues: true,
                showLegend: false,
                rightAlignYAxis: true,
                callback: function(chart) {
                    //console.log("!!! fingerTapChartOptions callback !!!");
                }
            }
        };

        /**
         * [fingerTapChartOptions]
         * @type {Object}
         */
        $scope.fingerTapChartOptions = {
            chart: {
                name: 'tapChart',
                type: 'multiBarChart',
                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + moment(graph.point.x).format('HH:mm') + '</h3>' + y + '  taps';
                },
                height: 200,
                margin: {
                    top: 20,
                    right: 30,
                    bottom: 40,
                    left: 20
                },
                legend: {
                    width: 100,
                    height: 30,
                    align: true,
                    rightAlign: false,
                    margin: {
                        top: 5,
                        right: 0,
                        bottom: 15,
                        left: 0
                    },
                },
                noData: noDataText,
                valueFormat: d3.format(',f'),
                tickFormat: d3.format(',f'),
                duration: 500,
                reduceXTicks: false,
                groupSpacing: 0.5,
                staggerLabels: false,
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    tickFormat: function(d) {
                        return parseInt(d);
                    },
                    axisLabel: 'Taps'
                },
                showControls: false,
                showXAxis: true,
                showYAxis: true,
                showLegend: true,
                rightAlignYAxis: true,
                callback: function(chart) {
                    //console.log("!!! fingerTapChartOptions callback !!!");
                }
            }
        };

        $scope.tapAverageGraphPopup = function() {

            var header = $filter('translate')('Veckovis Fingertap översikt');
            var body = '<p>I det här digrammet ser du en sammanfattning över veckan som gått. Det visar ett medelvärde för respektive höger och vänster hand, samt högsta och lägsta värde under veckan. <br><br> Linjediagrammet visar hela veckans genomsnitt. Det du vill uppnå här är att linjerna ska hålla sig så parallella som möjligt. Antalet tappar är inte det viktiga utan den parallella linjerna visar att du är jämn.</p>';
            var button1 = $filter('translate')('CLOSE');

            tapAveragePopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
                    onTap: function(e) {
                        console.log('card closed');
                        tapAveragePopup.close();
                    }
                }]
            });
            tapAveragePopup.then(function(res) {
                console.log('done');
            });
        };

        $scope.tapsGraphPopup = function() {

            var header = $filter('translate')('Fingertap diagram');
            var body = '<p>I det här diagrammet ser du dagens Fingertap omgång. Här kan du jämföra höger och vänster hand under dagens gång.</p>';
            var button1 = $filter('translate')('CLOSE');

            tapsPopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
                    onTap: function(e) {
                        console.log('card closed');
                        tapsPopup.close();
                    }
                }]
            });
            tapsPopup.then(function(res) {
                console.log('done');
            });
        };


        $scope.slideBoxUpdate = function(thefunc) {
            console.log(thefunc, moment().format('MMMM Do YYYY, h:mm:ss a'));
            if (($ionicSlideBoxDelegate.slidesCount() === 0) || ($ionicSlideBoxDelegate.currentIndex() != 6)) {
                $ionicSlideBoxDelegate.slide(6);
                console.log('The SLIDE! ', moment().format('MMMM Do YYYY, h:mm:ss a'));
            }
        };

    });
