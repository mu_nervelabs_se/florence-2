angular.module('florence.controllers')

  .controller('ProfileCtrl-2', function(
    $scope, $cordovaCamera, User, $ionicPopup, apiService, $state, $filter,
    PouchDBService, $rootScope) {

    User.get().then(function(user) {
      $scope.user = user;
    });

    $scope.profile = {};

    PouchDBService.getProfileDB().get('profile').then(function(data) {

      $scope.profile = data;
      $scope.profile.birthdate = moment($scope.profile.birthdate).toDate();

      if (_.isEmpty(data.medicineDB)) {
        $scope.profile.medicineDB = $rootScope.medicineDB;
      } else {
        $rootScope.medicineDB = data.medicineDB;
      }
    });

    apiService.class.availableTags().success(function(data) {
      $scope.availableTags = data;
    });

    $scope.saveProfile = function() {

      $rootScope.medicineDB = $scope.profile.medicineDB;
      if (!$scope.profile || !$scope.profile._id) {
        $scope.profile._id = 'profile';
      }
      PouchDBService.getProfileDB().put($scope.profile).then(function(data) {
        $scope.profile._rev = data.rev;
        console.log("saved in profile!");
      });

      //User.updateUser($scope.user);
    };

    $scope.back = function() {
      $state.go('tab.deck');
    }
    $scope.toggleSelectTag = function(tag) {
      $scope.profile.tags = $scope.profile.tags || [];

      if (_.find($scope.profile.tags, {
          'id': tag.id
        })) {
        _.remove($scope.profile.tags, {
          'id': tag.id
        });
      } else {
        $scope.profile.tags.push(tag);
      }
    };

    $scope.checkIfIsActive = function(tag) {
      return _.find($scope.profile.tags, {
        'id': tag.id
      });
    };

    $scope.logout = function() {
      User.logout().then(function() {
        $state.go('tab.auth');
      });
    };

    $scope.sendPasswordResetMail = function() {
      apiService.user.sendPasswordResetMail($scope.user.email)
        .success(function(data) {
          $scope.emailsentPopUp();
        }).error(function(data, status) {
          $scope.error = 'HTTP_' + status;
        });
    };

    $scope.resetConfirmPopUp = function() {
      var header = $filter('translate')('RESET_PASSWORD');
      var body = $filter('translate')('PASSWORD_RESET_CONFIRM');
      var CANCEL_string = $filter('translate')('CANCEL');
      var RESET_string = $filter('translate')('RESET');

      resetPopup = $ionicPopup.show({
        title: header,
        template: body,
        scope: $scope,
        buttons: [{
          type: 'button button-clear button-assertive popup-Buttons',
          text: CANCEL_string,
          onTap: function(e) {
            resetPopup.close();
          }
        }, {
          type: 'button button-clear button-balanced popup-Buttons',
          text: RESET_string,
          onTap: function(e) {
            $scope.sendPasswordResetMail();
            resetPopup.close();
          }
        }]
      });
      resetPopup.then(function(res) {});
    };

    $scope.emailsentPopUp = function() {
      var header = $filter('translate')('RESET_PASSWORD');
      var body = $filter('translate')('FORGOT_PASSWORD_SENT');
      emailPopup = $ionicPopup.show({
        title: header,
        template: body,
        scope: $scope,
        buttons: [{
          type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
          onTap: function(e) {
            $scope.logout();
            emailPopup.close();
          }
        }]
      });
      emailPopup.then(function(res) {});
    };

    $scope.passwordChangeSuccessPopup = function() {
      var header = $filter('translate')('CHANGE_PASSWORD');
      var body = $filter('translate')('CHANGE_PASSWORD_SUCCESS');
      emailPopup = $ionicPopup.show({
        title: header,
        template: body,
        scope: $scope,
        buttons: [{
          type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
          onTap: function(e) {
            $scope.logout();
            emailPopup.close();
          }
        }]
      });
      emailPopup.then(function(res) {});
    };


    $scope.changePasswordData = {};
    $scope.changePassword = function(changePasswordData) {
      $scope.changePasswordMessage = null;
      apiService.user.changePassword(changePasswordData)
        .success(function() {
          $scope.passwordChangeSuccessPopup();
        })
        .error(function(data) {
          $scope.changePasswordMessage = data.translationValue;
        });
    };

    $scope.newEmail = {};
    $scope.changeEmailAddress = function(newEmail) {
      $scope.changeEmailMessage = null;
      apiService.user.changeEmailAddress(newEmail)
        .success(function(data) {
          $scope.user.email = data.email;
          User.updateUser(data);
          $rootScope.$broadcast('showFrontPanel');
        })
        .error(function(data) {
          $scope.changeEmailMessage = data.translationValue;
        });
    };

    /**
     * Avatar picture handling
     */
    if (navigator.camera) {

      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 256,
        targetHeight: 256,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        cameraDirection: Camera.Direction.FRONT
      };

      $scope.pictureSourcePopUp = function() {
        var header = $filter('translate')('ADD_IMAGE');
        imagesourcePopup = $ionicPopup.show({
          title: header,
          template: '<button class="button ion-ios-camera-outline popup-photoAdd" ng-click="getPhoto(' + "'CAMERA'" + ')"></button><button class="button ion-ios-photos-outline popup-photoAdd" ng-click="getPhoto(' + "'PHOTOLIBRARY'" + ')"></button>',
          scope: $scope,
          buttons: [{
            type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
            onTap: function(e) {
              imagesourcePopup.close();
            }
          }]
        });
        imagesourcePopup.then(function(res) {});
      };

      $scope.getPhoto = function(source) {
        if (source == 'CAMERA') {
          options.sourceType = Camera.PictureSourceType.CAMERA;
        } else {
          options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
        }
        $cordovaCamera.getPicture(options).then(function(imageData) {
          $scope.myImage = imageData;
          $scope.profile.avatar = imageData;
          imagesourcePopup.close();
        }, function(err) {});
      };
    }
  });
