angular.module('florence.controllers')

    .controller('SleepChartCtrl', function($scope, $ionicPopup, apiService, $filter, $state, $ionicSlideBoxDelegate, $timeout) {

        $scope.sleepReady = false;

        $scope.showPopupLoading = function() {
            popupLoading = $ionicPopup.show({
                title: $filter('translate')('LOADING'),
                template: '<ion-spinner class="spinner-dark" icon="lines"></ion-spinner>',
                scope: $scope
            });
        }

        $scope.showPopupError = function() {
            popupError = $ionicPopup.show({
                title: $filter('translate')('MISFIT_CONNECTION_ERROR'),
                buttons: [{
                    type: 'button button-clear button-assertive popup-buttons',
                    text: 'ok',
                    onTap: function(e) {
                        $state.go('tab.deck');
                    }
                }],
                scope: $scope
            });
        }

        console.log('SleepChartCtrl START', moment().format('MMMM Do YYYY, h:mm:ss a'));
        //Time range: last 7 days.
        var start_date = moment().subtract(6, 'days').format('YYYY-MM-DD');
        var end_date = moment().subtract(0, 'days').format('YYYY-MM-DD');

        //We prefilled with days
        $scope.charts = {};
        for (var i = 6; i >= 0; i--) {
            var day = moment().subtract(i, 'days').format('YYYY-MM-DD');
            $scope.charts[day] = {};
        }

        /**
         * [getSleep description]
         * @return {[type]}
         */
        getSleep = function() {
            $scope.showPopupLoading();

            apiService.misfit.getSleep({
                start_date: start_date,
                end_date: end_date
            }).success(function(data) {
                popupLoading.close();
                //console.log(JSON.stringify(data));
                //missfit provide "agregated sleep sessions" (sleepdetails within sleeps), we create first  all sleepsessions fron the missfit data.
                var sleepSessions = [];

                _.forEach(data.sleeps, function(data, sleep) {
                    // misfit sleep provide a startTime an duration, we calculate Endtime, that will be the last sleepSession endTime
                    var startTime = parseInt(moment(data.startTime).format('X'));
                    var endTime = parseInt(moment(data.startTime).add(data.duration, 's').format('X'));


                    _.forEach(data.sleepDetails, function(sleepDetail, key) {
                        var nextKey = key + 1;
                        var sessionStartTime = parseInt(moment(sleepDetail.datetime).format('X'));
                        var sessionEndTime;
                        if (data.sleepDetails.hasOwnProperty(nextKey)) {

                            sessionEndTime = parseInt(moment(data.sleepDetails[nextKey].datetime).format('X'));
                        } else {
                            sessionEndTime = endTime;

                        }
                        var sessionDuration = sessionEndTime - sessionStartTime;

                        //sleepSeesion data
                        sleepSession = {
                            startTime: sessionStartTime,
                            endTime: sessionEndTime,
                            duration: sessionDuration,
                            quality: sleepDetail.value,
                            id: data.id
                        };
                        sleepSessions.push(sleepSession);
                    });
                });
                for (i = 0; i < sleepSessions.length; i++) {

                    var newStart = moment(sleepSessions[i].startTime * 1000).add(12, 'hours');
                    var newEnd = moment(sleepSessions[i].endTime * 1000).add(12, 'hours');

                    sleepSessions[i].startTime = newStart;
                    sleepSessions[i].endTime = newEnd;

                }

                //console.log(JSON.stringify(sleepSessions));
                var sleepData = [];
                var AsleepAverage = [];
                var DsleepAverage = [];
                var LsleepAverage = [];

                var sleepByDay = _.groupBy(sleepSessions, function(it) {
                    return moment(it.startTime).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                });
                //console.log(JSON.stringify(sleepByDay));


                _.forEach($scope.charts, function(data, day) {

                    var Dsleep = [];
                    var Lsleep = [];
                    var Asleep = [];
                    var DsleepCount = 0;
                    var LsleepCount = 0;
                    var AsleepCount = 0;
                    var sleepCount = 0;

                    $scope.charts[day].sleepChart = {};
                    $scope.charts[day].date = moment(day).format('LL');


                    if (sleepByDay[day]) {
                        sleepByDay[day] = _.sortBy(sleepByDay[day], 'data.startTime');

                        var i = 0;

                        _.forEach(sleepByDay[day], function(data, session) {
                            var currentID = data.id;

                            sessionStart = moment(data.startTime);
                            sessionEnd = moment(data.endTime);

                            sessionStart = sessionStart.format('HH:mm');
                            sessionEnd = sessionEnd.format('HH:mm');

                            var relativeSessionStart = moment(data.startTime).diff(moment(day)) - (moment(0).utcOffset() * 60000);

                            var deepSleepGrade = 0;
                            var lightSleepGrade = 0;
                            var awakeSleepGrade = 0;
                            var sleepQuality = parseInt(data.quality);
                            //alert(sleepQuality);
                            if (sleepQuality == 1) {
                                awakeSleepGrade = 3;
                                AsleepCount = AsleepCount + parseInt(data.duration);
                            };
                            if (sleepQuality == 2) {
                                lightSleepGrade = 3;
                                LsleepCount = LsleepCount + parseInt(data.duration);
                            };
                            if (sleepQuality == 3) {
                                deepSleepGrade = 3;
                                DsleepCount = DsleepCount + parseInt(data.duration);
                            };
                            sleepCount = sleepCount + parseInt(data.duration);

                            Dsleep.push({
                                x: moment(relativeSessionStart).subtract(12, 'hours'),
                                y: deepSleepGrade,
                                size: data.duration,
                                id: currentID
                            });
                            Lsleep.push({
                                x: moment(relativeSessionStart).subtract(12, 'hours'),
                                y: lightSleepGrade,
                                size: data.duration,
                                id: currentID
                            });
                            Asleep.push({
                                x: moment(relativeSessionStart).subtract(12, 'hours'),
                                y: awakeSleepGrade,
                                size: data.duration,
                                id: currentID
                            });
                        });

                        AsleepAverage.push({
                            y: (AsleepCount / 60) / 60,
                            x: moment(day)
                        });
                        DsleepAverage.push({
                            y: (DsleepCount / 60) / 60,
                            x: moment(day)
                        });
                        LsleepAverage.push({
                            y: (LsleepCount / 60) / 60,
                            x: moment(day)
                        });

                        $scope.charts[day].deepSleepTotal = parseInt((DsleepCount / 60) / 60) + 'h' + parseInt((((DsleepCount / 60) / 60) - parseInt((DsleepCount / 60) / 60)) * 60) + 'm';
                        $scope.charts[day].lightSleepTotal = parseInt((LsleepCount / 60) / 60) + 'h' + parseInt((((LsleepCount / 60) / 60) - parseInt((LsleepCount / 60) / 60)) * 60) + 'm';
                        $scope.charts[day].awakeSleepTotal = parseInt((AsleepCount / 60) / 60) + 'h' + parseInt((((AsleepCount / 60) / 60) - parseInt((AsleepCount / 60) / 60)) * 60) + 'm';
                        $scope.charts[day].SleepTotal = parseInt((sleepCount / 60) / 60) + 'h ' + parseInt((((sleepCount / 60) / 60) - parseInt((sleepCount / 60) / 60)) * 60) + 'm';

                        //alert($scope.charts[day].deepSleepTotal + ' ' + $scope.charts[day].lightSleepTotal + ' ' + $scope.charts[day].awakeSleepTotal);

                        //alert(JSON.stringify(sleepData));
                        data = [{
                            values: Dsleep,
                            key: 'Deep Sleep',
                            color: '#5C6BC0'
                        }, {
                            values: Lsleep,
                            key: 'Light Sleep',
                            color: '#303F9F'
                        }, {
                            values: Asleep,
                            key: 'Awake',
                            color: '#8BC34A'
                        }];
                        $scope.weekSleep = sleepData;
                        $scope.charts[day].sleepChart.data = data;
                        //console.log(JSON.stringify(data));
                        data = [];
                    } else {
                        $scope.charts[day].sleepChart.data = [];
                    }
                    ++i;
                    sleepData = [{
                        values: AsleepAverage,
                        key: 'Awake Sleep',
                        color: '#8BC34A'
                    }, {
                        values: LsleepAverage,
                        key: 'Light Sleep',
                        color: '#5C6BC0'
                    }, {
                        values: DsleepAverage,
                        key: 'Deep Sleep',
                        color: '#303F9F'
                    }];
                });
                $scope.slideBoxUpdate('getSleep');
            }).error(function(error) {
                popupLoading.close();
                $scope.showPopupError();
            });
        }
        getSleep();
        /**
         * [sleepOptions description]
         * @type {Object}
         */
        $scope.sleepOptions = {
            chart: {
                type: 'stackedAreaChart',

                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + graph.point.sessionStart + ' - ' + graph.point.sessionEnd + '</h3>' + x + ': Level ' + y;
                },

                height: 150,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 20
                },
                legend: {
                    margin: {
                        bottom: 20
                    }
                },
                duration: 500,
                reduceXTicks: false,
                interpolate: "step-before",
                showControls: false,
                showLegend: false,
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 0
                },
                yAxis: {
                    axisLabel: 'Sleep Quality'
                },
                showYAxis: false,
                callback: function(chart) {
                    // console.log("!!! sleepChart callback !!!");
                }
            }
        };

        $scope.weekSleepChartOptions = {
            chart: {
                type: 'stackedAreaChart',
                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + moment(graph.point.x).format('HH:mm') + '</h3>' + y + '  taps';
                },
                interpolate: 'cardinal',
                height: 200,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 20
                },

                duration: 500,
                reduceXTicks: false,
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('dd');
                    },
                    rotateLabels: 0
                },
                yAxis: {
                    tickFormat: function(y) {
                        return y
                    }
                },
                showControls: false,
                showXAxis: true,
                showYAxis: true,
                showValues: true,
                showLegend: false,
                rightAlignYAxis: true,
                callback: function(chart) {
                    //console.log("!!! fingerTapChartOptions callback !!!");
                }
            }
        };

        /**
         * [slideBoxUpdate description]
         * @param  {[type]}
         * @return {[type]}
         */
        $scope.slideBoxUpdate = function(thefunc) {
            console.log(thefunc, moment().format('MMMM Do YYYY, h:mm:ss a'));
            if (($ionicSlideBoxDelegate.slidesCount() === 0) || ($ionicSlideBoxDelegate.currentIndex() != 6)) {
                $ionicSlideBoxDelegate.slide(6);
                $scope.sleepReady = true;
                console.log('The SLIDE! ', moment().format('MMMM Do YYYY, h:mm:ss a'));
            }
        };

    });
