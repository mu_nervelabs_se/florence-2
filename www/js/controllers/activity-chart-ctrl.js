angular.module('florence.controllers')

    .controller('ActivityChartCtrl', function($scope, apiService, $state, $ionicSlideBoxDelegate, $timeout, $filter) {

        console.log('ActivityChartCtrl START', moment().format('MMMM Do YYYY, h:mm:ss a'));
        //Time range: last 7 days.
        var start_date = moment().subtract(6, 'days').format('YYYY-MM-DD');
        var end_date = moment().subtract(0, 'days').format('YYYY-MM-DD');

        //We prefilled with days
        $scope.charts = {};
        for (var i = 6; i >= 0; i--) {
            var day = moment().subtract(i, 'days').format('YYYY-MM-DD');
            $scope.charts[day] = {};
        }

        //we force Xaxis to min/max for line charts
        var forceXmin = 0 - (moment(0).utcOffset() * 60000);
        var forceXmax = 86399999 - (moment(0).utcOffset() * 60000);

        // Debug filter with state, background load with refresh option should be better.
        getActivity();

        /**
         * [getActivity description]
         * @return {[type]}
         */
        function getActivity() {

            apiService.misfit.getSessions({
                start_date: start_date,
                end_date: end_date
            }).success(function(data) {

                //console.log(JSON.stringify(data));
                var aveData = [];
                var aveActivity = [];
                var activityByDay = _.groupBy(data.sessions, function(it) {
                    return moment(it.startTime).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                });
                _.forEach($scope.charts, function(data, day) {

                    var activity = [];
                    var noActivity = [];
                    var dayStart = moment(day).startOf('day');
                    dayStart = moment(dayStart).set('h', 00);
                    dayStart = moment(dayStart).set('m', 00);
                    dayStart = moment(dayStart).set('s', 00);
                    $scope.charts[day].calories = 0;
                    $scope.charts[day].distance = 0;
                    $scope.charts[day].steps = 0;

                    for (index = 1; index < 145; index++) {
                        var addTime = moment(dayStart).add(10 * index, 'm');
                        noActivity.push({
                            x: addTime,
                            y: 50,
                            size: 0,
                        });
                    };

                    //console.log(JSON.stringify(noActivity));

                    $scope.charts[day].activityChart = {};
                    $scope.charts[day].date = moment(day).format('LL');

                    if (activityByDay[day]) {

                        activityByDay[day] = _.sortBy(activityByDay[day], 'data.startTime');


                        _.forEach(activityByDay[day], function(data, session) {


                            sessionStart = moment(data.startTime).diff(moment(day)) - (moment(0).utcOffset() * 60000);
                            sessionEnd = moment(sessionStart).add(data.duration, 's');

                            sessionMid = moment(data.startTime).add(data.duration / 2, 's');
                            //alert(moment(sessionMid));

                            //sessionStart = sessionStart.format('HH:mm');
                            //sessionEnd = sessionEnd.format('HH:mm');

                            var relativeSessionStart = moment(data.startTime).diff(moment(day)) - (moment(0).utcOffset() * 60000);

                            var newMinutes = moment(sessionMid).get('m');
                            if (newMinutes > 57) {
                                sessionMid = moment(sessionMid).set('m', 00);
                                sessionMid = moment(sessionMid).add(1, 'h');
                                sessionMid = moment(sessionMid).set('s', 00);

                            } else {
                                newMinutes = 10 * Math.round(newMinutes / 10);
                                sessionMid = moment(sessionMid).set('m', newMinutes);
                                sessionMid = moment(sessionMid).set('s', 00);
                            }


                            var timeCount = parseInt(data.duration / 600) + 3;
                            if (timeCount < 1) {
                                timeCount = 1
                            };
                            var timeInt = 0;


                            for (i = 0; i < timeCount; i++) {
                                activity.push({
                                    x: moment(sessionMid).add(timeInt, 's'),
                                    y: data.steps / (timeCount - i),
                                    size: data.duration,

                                });
                                timeInt = timeInt + 600;
                            }

                            for (i = 0; i < timeCount; i++) {
                                activity.push({
                                    x: moment(sessionMid).add(timeInt, 's'),
                                    y: data.steps - (data.steps / (timeCount - i)),
                                    size: data.duration,
                                });
                                timeInt = timeInt + 600;
                            }

                        });
                        // alert(moment(activity[i].x).format('H HH') + ' ' + moment(noActivity[index].x).format('H HH'));

                        for (var i = 0, iL = activity.length; i < iL; i++) {

                            var placeholder = (moment(activity[i].x).get('m') / 10) + (moment(activity[i].x).get('h') * 6);
                            noActivity[placeholder] = activity[i];

                        };

                        data = [{
                            values: noActivity, //values - represents the array of {x,y} data points
                            key: 'activity', //key  - the name of the series.
                            color: '#ffc107',
                        }];

                        $scope.charts[day].activityChart.data = data;
                        data = [];

                    } else {
                        $scope.charts[day].activityChart.data = [];
                    }

                    var sumStart_date = moment(day).format('YYYY-MM-DD');
                    var sumEnd_date = moment(day).format('YYYY-MM-DD');

                    apiService.misfit.getSummary({
                        start_date: sumStart_date,
                        end_date: sumEnd_date
                    }).success(function(sumData) {


                        $scope.charts[day].calories = parseInt(sumData.calories);
                        $scope.charts[day].distance = Math.round(sumData.distance * 100) / 100;
                        $scope.charts[day].steps = sumData.steps;

                        aveActivity.push({
                            x: moment(day),
                            y: sumData.steps
                        });

                    });
                });


                aveActivity = _.sortBy(aveActivity, 'x');

                aveData = [{
                    values: aveActivity, //values - represents the array of {x,y} data points
                    key: 'activity', //key  - the name of the series.
                    color: '#ffc107',
                }];

                $scope.aveActivityChart = aveData;


                $scope.slideBoxUpdate('getActivity');
            }).error(function(error) {
                console.log('no connection');
            });
        }




        /**
         * [activityOptions description]
         * @type {Object}
         */
        $scope.activityOptions = {
            chart: {
                type: 'multiBarChart',

                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + graph.point.sessionStart + ' - ' + graph.point.sessionEnd + '</h3>' + x + ': Level ' + y;
                },

                height: 150,
                margin: {
                    top: 20,
                    right: 0,
                    bottom: 20,
                    left: 0
                },
                groupSpacing: 0.5,
                legend: {
                    margin: {
                        bottom: 20
                    }
                },
                duration: 500,
                reduceXTicks: true,
                interpolate: "basis",
                showControls: false,
                showLegend: false,
                forceX: [forceXmin, forceXmax],
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 0
                },
                yAxis: {
                    axisLabel: 'Sleep Quality'
                },
                showYAxis: false,
                callback: function(chart) {
                    // console.log("!!! sleepChart callback !!!");
                }
            }
        };

        $scope.averageActivityChartOptions = {
            chart: {
                type: 'lineChart',
                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + moment(graph.point.x).format('HH:mm') + '</h3>' + y + '  taps';
                },
                interpolate: 'basis',
                height: 170,
                margin: {
                    top: 20,
                    right: 30,
                    bottom: 20,
                    left: 30
                },
                legend: {
                    width: 100,
                    height: 30,
                    align: true,
                    rightAlign: false
                },
                duration: 500,
                reduceXTicks: false,
                staggerLabels: false,
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('dd');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    axisLabel: 'Steps'
                },
                showControls: false,
                showXAxis: true,
                showYAxis: true,
                showValues: true,
                showLegend: false,
                rightAlignYAxis: true,
                callback: function(chart) {
                    //console.log("!!! fingerTapChartOptions callback !!!");
                }
            }
        };

        /**
         * [slideBoxUpdate description]
         * @param  {[type]}
         * @return {[type]}
         */
        $scope.slideBoxUpdate = function(thefunc) {
            console.log(thefunc, moment().format('MMMM Do YYYY, h:mm:ss a'));
            if (($ionicSlideBoxDelegate.slidesCount() === 0) || ($ionicSlideBoxDelegate.currentIndex() != 6)) {
                $ionicSlideBoxDelegate.slide(6);
                console.log('The SLIDE! ', moment().format('MMMM Do YYYY, h:mm:ss a'));
            }
        };

    });
