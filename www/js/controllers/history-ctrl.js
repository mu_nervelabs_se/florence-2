angular.module('florence.controllers')

    .controller('HistoryCtrl', function($scope, Intakes, $rootScope, $filter, historyService, $ionicSlideBoxDelegate) {
        var allIntakes;
        $scope.intake = {};
        $scope.intake.date = new Date();
        $scope.activeHistoryEntry = undefined;
        $scope.groupedIntakes = [];
        $scope.dosages = [0.25, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        $scope.pain = {};
        $scope.pain.types = ["DULL", "BURNING", "SCURRYING", "INCISIVE", "PULSATING", "PUNGENT", "SWELTERING", "OTHER_PAIN"];
        $scope.pain.typeItems = [];

        $scope.selectPainType = function(selected) {
            $scope.activeHistoryEntry.intakes[0].doc.painType = selected;
            $scope.pain.typeItems = [];
        };
        $scope.showPainType = function() {
            $scope.pain.typeItems = $scope.pain.types;
        };
        $scope.initIntakes = function() {
            $scope.groupedIntakes = [];
            Intakes.getIntakes().then(function(response) {

                response = _.filter(response, function(it) {
                    if (historyService.filterSingleDocByKind(it.doc, $scope.filters)) {
                        return it;
                    }
                });

                $scope.groupedIntakes = historyService.groupIntakesByDayAndTime(response);
                historyService.padListWithEmptyHistoryDays($scope.groupedIntakes);
                $scope.intakes = response;
                $scope.$apply();
            });
            $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.goToDeck = function() {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.toggleFilterDisplay = function() {
            $scope.filterOpen = !$scope.filterOpen;
        };

        $scope.$on('refreshHistory', function(e) {
            $scope.initIntakes();
            historyService.initFiltersOnScope($scope);
        });

        $scope.doIntakeFilter = function(object) {
            if (object.intakes[0] && historyService.filterSingleDocByKind(object.intakes[0].doc, $scope.filters)) {
                return object;
            }
        };

        $scope.initTakenDate = function(TS) {
            return moment(TS * 1000).toDate();
        };

        historyService.initFiltersOnScope($scope);

        $scope.deleteIntakeFromList = historyService.deleteIntakeFromList.bind(historyService);
        $scope.deleteIntake = historyService.deleteIntake.bind(historyService, function() {
            $scope.initIntakes();
        });
        $scope.deleteAllIntakes = historyService.deleteAllIntakes.bind(historyService);
        $scope.updateIntake = historyService.updateIntake.bind(historyService);
        $scope.addTestIntake = historyService.addTestIntake.bind(historyService);

        $scope.setActiveIntakes = function(intake) {
            $scope.activeHistoryEntry = intake;
            $scope.activeAccordeon = null;
            $scope.takenTime = intake.intakes[0].doc.takenHour + ':' + intake.intakes[0].doc.takenMinute;
        };

        $scope.selectDosage = function(intake, selected) {
            intake.doc.dosage = selected;
            $scope.dosageOpen = !$scope.dosageOpen;
            Intakes.updateIntake(intake).then(function(response) {
                intake.doc._rev = response.rev;
            });
        };

        $scope.showDosage = function() {
            $scope.dosageOpen = !$scope.dosageOpen;
        };

        $scope.exportCSV = function() {
            historyService.exportAsCSV($scope.filters);
        };


        $scope.blankReplace = function(intake, type) {
            if (type == 'hour') {
                if (intake.doc.takenHour === '') {
                    intake.doc.takenHour = $scope.tempHour;
                }
                if (intake.doc.takenHour.length < 2) {
                    intake.doc.takenHour = '0' + intake.doc.takenHour;
                }
            }
            if (type == 'minute') {
                if (intake.doc.takenMinute === '') {
                    intake.doc.takenMinute = $scope.tempMinute;
                }
                if (intake.doc.takenMinute.length < 2) {
                    intake.doc.takenMinute = '0' + intake.doc.takenMinute;
                }
            }
        };

        $scope.clearField = function(intake, type) {

            if (type == 'hour') {
                $scope.tempHour = intake.doc.takenHour;
                intake.doc.takenHour = '';
            }
            if (type == 'minute') {
                $scope.tempMinute = intake.doc.takenMinute;
                intake.doc.takenMinute = '';
            }
        };

        $scope.toggleActive = function(intake) {
            intake.doc.checked = !intake.doc.checked;
            $scope.updateIntake(intake);
        };


        $scope.openAccordeon = function(index) {
            if ($scope.activeAccordeon == index) {
                $scope.activeAccordeon = null;
                return;
            }
            $scope.activeAccordeon = index;
        };

        $scope.save = function(intake) {
            Intakes.updateIntake(intake).then(function(response) {
                intake.doc._rev = response.rev;
                $scope.back();
            });
        };


        $scope.back = function() {
            $rootScope.$broadcast('showFrontPanel');
            $scope.initIntakes();
        };

        $scope.initIntakes();
    });
