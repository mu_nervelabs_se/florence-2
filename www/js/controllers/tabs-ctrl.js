angular.module('florence.controllers')

    .controller('TabsCtrl', function($scope, $ionicModal, $ionicPopup, $ionicPlatform, Intakes, $rootScope, Reminders, $timeout, $filter) {

        /**
         * hold hours and minutes of the postpone
         * @type {Object}
         */
        $scope.postponeData = {};

        /**
         *  Notification Modal settings
         */
        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'none'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.addIntake();
        };

        /**
         * Open a modal containing the reminder medicines
         */
        var openReminder = function() {

            $scope.notification = angular.copy($rootScope.notificationProperties);
            delete $rootScope.notificationProperties;

            $scope.notification.data = JSON.parse($scope.notification.data);

            //call recent intakes that are plus ones in x hours
            Intakes.getRecent(6, 'Plusone').then(function(response) {
                var recentIntakes = response;

                _.forEach($scope.notification.data.medicines, function(medecine) {
                    medecine.checked = false;
                    for (i = 0; i < recentIntakes.length; ++i) {
                        if (medecine.nplid == recentIntakes[i].doc.nplid) {
                            medecine.prevTaken = true;
                            medecine.prevTakenTime = moment(recentIntakes[i].doc.date).fromNow();
                        }
                    }
                });
            });
            $scope.postponeActive = false;
            $scope.openModal();
        };

        /**
         * Conditions to open the reminder modal
         */
        $scope.$on('notificationClick', function() {
            openReminder();
        });


        $scope.addIntake = function() {
            console.log('addIntake');
            _.forEach($scope.notification.data.medicines, function(intakeMedicine) {
                console.log('Intake Added:');
                Intakes.addBasicIntakeData(intakeMedicine, $scope.notification);
                Intakes.addIntake(intakeMedicine).then(function(response) {
                    console.log(angular.toJson(response, true));
                });
            });
            $scope.modal.hide();
            $rootScope.$broadcast('initDeck');
        };

        $scope.saveAndPostpone = function() {
            console.log('saveAndPostpone');
            var checkedMedicines = _.filter($scope.notification.data.medicines, {
                checked: true
            });
            var uncheckedMedicines = _.filter($scope.notification.data.medicines, {
                checked: false
            });

            Reminders.addNewPostponeNotification($scope.notification, uncheckedMedicines, $scope.postponeData);

            console.log('Intake Added:');
            _.forEach(checkedMedicines, function(intakeMedicine) {
                Intakes.addBasicIntakeData(intakeMedicine, $scope.notification);
                Intakes.addIntake(intakeMedicine).then(function(response) {
                    console.log(angular.toJson(response, true));
                });
            });
            $scope.modal.hide();
            $rootScope.$broadcast('initDeck');
        };

        $scope.prevTakenAlert = function(medicine) {

            if (medicine.prevTaken && !medicine.checked) {
                var header = $filter('translate')('ADD_TAKEN_MEDICATION');
                var body = $filter('translate')('MEDICATION_ALREADY_TAKEN');
                var body2 = $filter('translate')('DO_YOU_WANT_TO_TAKE_MEDICATION_AGAIN');
                var button1 = $filter('translate')('NO');
                var button2 = $filter('translate')('YES');

                reminderPrevMed = $ionicPopup.show({
                    title: header,
                    template: '<p style="text-align:center;">' + body + '</p><h3 style="color:#000;margin-bottom:20px;">' + medicine.prevTakenTime + '</h3><p style="text-align:center;">' + body2 + '</p>',
                    scope: $scope,
                    buttons: [{
                        type: 'button button-clear button-assertive popup-Buttons',
                        text: button1,
                        onTap: function(e) {
                            medicine.checked = false;
                            console.log('medicine not added as taken');
                            reminderPrevMed.close();
                        }
                    }, {
                        type: 'button button-clear button-balanced popup-Buttons',
                        text: button2,
                        onTap: function(e) {
                            $timeout(function() {
                                medicine.checked = true;
                                console.log('medicine added as taken');
                            }, 0);
                            reminderPrevMed.close();
                        }
                    }]
                });
                reminderPrevMed.then(function(res) {
                    console.log('done');
                });
            } else {
                medicine.checked = !medicine.checked;
            }
        };

        $scope.changeTime = function(type) {
            if (type == 'hour') {
                $scope.postponeData.hour = $scope.postponeData.hour.replace(/\D+/g, '').substring(0, 2);
                if (parseInt($scope.postponeData.hour) > 23) {
                    $scope.postponeData.hour = 23;
                }
            }
            if (type == 'minute') {
                $scope.postponeData.minute = $scope.postponeData.minute.replace(/\D+/g, '').substring(0, 2);
                if (parseInt($scope.postponeData.minute) > 59) {
                    $scope.postponeData.minute = 59;
                }
            }
        };

        $scope.blankReplace = function(type) {
            if (type == 'hour') {
                if ($scope.postponeData.hour === '') {
                    $scope.postponeData.hour = $scope.tempHour;
                }
                if ($scope.postponeData.hour.length < 2) {
                    $scope.postponeData.hour = '0' + $scope.postponeData.hour;
                }
            }
            if (type == 'minute') {
                if ($scope.postponeData.minute === '') {
                    $scope.postponeData.minute = $scope.tempMinute;
                }
                if ($scope.postponeData.minute.length < 2) {
                    $scope.postponeData.minute = '0' + $scope.postponeData.minute;
                }
            }
        };

        $scope.clearField = function(type) {
            if (type == 'hour') {
                $scope.tempHour = $scope.postponeData.hour;
                $scope.postponeData.hour = '';
            }
            if (type == 'minute') {
                $scope.tempMinute = $scope.postponeData.minute;
                $scope.postponeData.minute = '';
            }
        };

        $scope.postpone = function() {
            var inFifteenMinutes = moment().add(15, 'minutes');
            $scope.postponeActive = !$scope.postponeActive;
            $scope.postponeData.hour = inFifteenMinutes.format('HH');
            $scope.postponeData.minute = inFifteenMinutes.format('mm');
        };

        $scope.areAnyUnchecked = function() {
            if ($scope.notification) {
                return _.some($scope.notification.data.medicines, {
                    checked: false
                });
            }
        };

        $scope.areAllUnchecked = function() {
            if ($scope.notification) {
                return !_.some($scope.notification.data.medicines, {
                    checked: true
                });
            }
        };
    });
