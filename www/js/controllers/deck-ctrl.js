angular.module('florence.controllers')

  .controller('DeckCtrl', function($scope, $rootScope, Reminders, Plusones, User, apiService, localStorageService, NotificationService, $timeout, PouchDBService, historyService, $ionicSlideBoxDelegate) {

    //needed in order to display user related data (Misfit data with token for example)
    User.get().then(function(user) {
      $scope.user = user;
    });
    PouchDBService.getProfileDB().get('profile').then(function(profile) {
      $scope.profile = profile;
      //console.log(data);
      console.log("+++++++++++++++++++++++++++++++++++++++++++++++++");
      console.log(profile);
    });

    $timeout(function() {
      console.log('deck-crtl.js timeout 200 scope.initDeck  trigger begin ---------');
      $scope.initDeck();
      console.log('deck-crtl.js timeout 200 scope.initDeck  trigger end   ---------');
    }, 200);

    $scope.initDeck = function() {

      Reminders.getAll().then(function(reminders) {
          $scope.reminders = reminders;
        },
        function(error) {
          console.error(error);
        },
        function(update) {
          console.log(update);
        });

      Plusones.getAll().then(function(response) {
        $scope.plusones = response;
      });


      PouchDBService.getProfileDB().get('profile').then(function(data) {



        if (!_.isEmpty(data.medicineDB)) {
          $rootScope.medicineDB = data.medicineDB;
        }

        apiService.class.getClasses(data.tags).success(function(data) {
          console.log(data.tags);
          localStorageService.set('classes', data);
          $scope.classes = data;
          //console.log($scope.classes);
        }).error(function() {
          $scope.classes = localStorageService.get('classes');
        });
      });





      historyService.getLastHistoryActivityDate().then(function(lastHistoryActivity) {
        historyService.generateEmptyHistoryEntries(lastHistoryActivity).then(function() {
          $timeout(function() {
            historyService.setLastHistoryActivityDate();
          }, 400);
        });

      });

    };

    $scope.refreshDeck = function() {
      console.log('Begin async operation');
      Reminders.getAll(true).then(function(response) {
        $scope.reminders = response;
      });
      Plusones.getAll(true).then(function(response) {
        $scope.plusones = response;
      });
      apiService.class.getClasses().success(function(data) {
        console.log(data.tags);
        localStorageService.set('classes', data);
        $scope.classes = data;
        console.log($scope.classes);
      }).error(function() {
        $scope.classes = localStorageService.get('classes');
      });


      if (window.cordova) {
        cordova.plugins.notification.local.getScheduled(function(notifications) {
          notifications = _.sortBy(notifications, 'id');
          _.forEach(notifications, function(notification) {
            console.log("Scheduled " + " " + moment(notification.at * 1000).format('MM-DD H:mm') + " " + notification.id + " " + notification.title.replace(/(?:\r\n|\r|\n)/g, ''));
          });
        });
        cordova.plugins.notification.local.getTriggered(function(notifications) {
          notifications = _.sortBy(notifications, 'id');
          _.forEach(notifications, function(notification) {
            console.log("Triggered" + " " + moment(notification.at * 1000).format('MM-DD H:mm') + " " + notification.id + " " + notification.title.replace(/(?:\r\n|\r|\n)/g, ''));
          });
        });
      }
      $timeout(function() {
        $scope.$broadcast('scroll.refreshComplete');
        console.log('Async operation has ended');
      }, 3000);

    };



    $scope.$on('$stateChangeSuccess', function(e, newVal) {
      console.log('on.stateChangeSuccess  ----------------------------------------------');
      if (newVal.name == 'tab.deck') {
        console.log('newVal.name == tab.deck is true-------------------------------');
        console.log('on.stateChangeSuccess scope.initDeck() triggered begin ---------');
        $scope.initDeck();
        console.log('on.stateChangeSuccess scope.initDeck() triggered end ---------');
      }
    });


    $scope.$on('remindersDBChange', function() {
      console.log('remindersDBChange --> UPDATE begin ---------------------');
      Reminders.getAll(true).then(function(response) {
        $scope.reminders = response;
      });
      console.log('remindersDBChange --> UPDATE end ---------------------');
    });

    $scope.$on('plusonesDBChange', function() {
      console.log('plusonesDBChange --> UPDATE begin ---------------------');
      Plusones.getAll(true).then(function(response) {
        $scope.plusones = response;
      });
      console.log('plusonesDBChange --> UPDATE end ---------------------');
    });
  });
