angular.module('florence.controllers')

    .controller('fingerTapCtrl', function($scope, User, $timeout, fingerTap, $location, $filter) {

        //Hold sessions data
        var sessions = {};

        //Hold the duration between each tap in millisec
        var taps = [];

        //Duration of the test in seconds
        var testDuration = 10;

        var testInit = function() {
            sessions = {};

            //while ready to start the test
            $scope.readyState = true;
            //while doing the test
            $scope.tapState = false;
            //while displaying result
            $scope.waitState = false;
            //while displaying result
            $scope.doneState = false;

            $scope.graph = false;

            $scope.Lhand = false;
            $scope.Rhand = false;
            $scope.timeLeft = $scope.progress = $scope.progressMax = testDuration;
            $scope.comunicate = $filter('translate')('SELECT HAND');
        };

        testInit();

        $scope.setLeftHand = function() {
            if ((!$scope.tapState) && (typeof sessions.l === 'undefined')) {
                $scope.Lhand = true;
                $scope.Rhand = false;
                hand = 'l';
                $scope.comunicate = $filter('translate')('TAP_TO_START');
            }
        };
        $scope.setRighHand = function() {
            if ((!$scope.tapState) && (typeof sessions.r === 'undefined')) {
                $scope.Lhand = false;
                $scope.Rhand = true;
                hand = 'r';
                $scope.comunicate = $filter('translate')('TAP_TO_START');
            }
        };

        $scope.logTap = function() {
            if ((typeof hand !== 'undefined') && !$scope.waitState) {
                $scope.readyState = false;
                $scope.tapState = true;
                current_time = (new Date()).getTime();
                //init
                if (taps.length === 0) {
                    sessionInit();
                }
                taps.push(current_time - startime);
                startime = current_time;
            }
        };

        //Tap Timeout, cf http://jsfiddle.net/dpeaep/LQGE2/1/
        onTimeout = function() {
            $scope.timeLeft--;
            $scope.progress = $scope.timeLeft - 1;
            if ($scope.timeLeft >= 0) {
                tapTimeout = $timeout(onTimeout, 1000);
            } else {
                tapTimeout = 0;
                $scope.tapState = false;
                $scope.waitState = true;

                //Prepare session data
                //Throw away first tap value
                taps.shift();
                var session = {
                    "userid": User.id(),
                    "hand": hand,
                    "speeds": taps,
                    "taps": taps.length,
                    "duration": testDuration,
                    "timestamp": new Date().getTime()
                };
                taps = session.taps;
                sessions[hand] = session;

                //Chart Data mapping
                $scope.labels = session.speeds.map(
                    function(element, index) {
                        return index + 1;
                    }
                );
                $scope.data.push(session.speeds);
            }
        };

        var sessionInit = function() {
            delete $scope.data;
            $scope.data = [];
            startime = current_time;
            var tapTimeout = $timeout(onTimeout, 1000);
            $scope.timeLeft = testDuration;
            $scope.progress = $scope.timeLeft - 1;
            $scope.comunicate = $filter('translate')('GO');

            $timeout(function() {
                $scope.comunicate = $filter('translate')('KEEP_GOING');
            }, testDuration * 1000 / 2);

            $timeout(function() {
                $scope.comunicate = $filter('translate')('GOOD_JOB');
                $scope.waitState = true;
                $scope.tapState = false;
                $scope.graph = true;
            }, testDuration * 1000);

            $timeout(function() {
                $scope.comunicate = taps + ' ' + $filter('translate')('TAPS');
                $scope.graph = true;
                taps = [];
            }, (testDuration * 1000) + 2000);

            $timeout(function() {
                $scope.timeLeft = $scope.progress = testDuration;
                if ((typeof sessions.r != 'undefined') && (typeof sessions.l != 'undefined')) {
                    $scope.Lhand = true;
                    $scope.Rhand = true;
                    $scope.doneState = true;
                    $scope.graph = false;
                    $scope.comunicate = $filter('translate')('SAVEEXIT');
                } else if (typeof sessions.l != 'undefined') {
                    $scope.setRighHand();
                    $scope.comunicate = $filter('translate')('NOWRIGHTHAND');
                    $scope.waitState = false;
                    $scope.graph = false;
                    $scope.readyState = true;
                } else if (typeof sessions.r != 'undefined') {
                    $scope.setLeftHand();
                    $scope.comunicate = $filter('translate')('NOWLEFTHAND');
                    $scope.waitState = false;
                    $scope.readyState = true;
                    $scope.graph = false;
                }
            }, (testDuration * 1000) + 4000);
        };

        $scope.close = function() {
            testInit();
            $location.path('tab/deck');
        };
        $scope.saveSession = function() {
            fingerTap.addSession(sessions.l).then(function(response) {

            });
            fingerTap.addSession(sessions.r).then(function(response) {

            });
            $scope.close();
        };

        // Chart settings
        $scope.labels = [];
        $scope.data = [];
        $scope.colours = [{
            fillColor: 'rgba(250,250,250,0.2)',
            strokeColor: 'rgba(250,250,250,1)',
            pointColor: "#7A12D6",
            pointStrokeColor: '#fff',
            pointHighlightFill: '#fff',
            pointHighlightStroke: '#7A12D6',
        }];
        $scope.options = {
            showScale: false,
            bezierCurve: true,
            datasetFill: true, //Radius of each point dot in pixels
            pointDotRadius: 0, //Pixel width of point dot stroke
            pointDotStrokeWidth: 2, //amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 5,
        };
    });
