angular.module('florence.controllers')

    .controller('Nvd3Ctrl', function($scope, apiService, fingerTap, Intakes, $state, $ionicSlideBoxDelegate, $timeout) {

        console.log('Nvd3Ctrl START', moment().format('MMMM Do YYYY, h:mm:ss a'));
        //Time range: last 7 days.
        var start_date = moment().subtract(6, 'days').format('YYYY-MM-DD');
        var end_date = moment().subtract(0, 'days').format('YYYY-MM-DD');

        //We prefilled with days
        $scope.charts = {};
        for (var i = 6; i >= 0; i--) {
            var day = moment().subtract(i, 'days').format('YYYY-MM-DD');
            $scope.charts[day] = {};
        }

        //we force Xaxis to min/max for line charts
        var forceXmin = 0 - (moment(0).utcOffset() * 60000);
        var forceXmax = 86399999 - (moment(0).utcOffset() * 60000);

        // Debug filter with state, background load with refresh option should be better.

        if ($state.current.name == 'tab.chart-activity') {
            getActivity();
            getSleep();
        }


        /**
         * [getSleep description]
         * @return {[type]}
         */
        function getSleep() {
            apiService.misfit.getSleep({
                start_date: start_date,
                end_date: end_date
            }).success(function(data) {
                //console.log(JSON.stringify(data));
                //missfit provide "agregated sleep sessions" (sleepdetails within sleeps), we create first  all sleepsessions fron the missfit data.

                var sleepSessions = [];

                _.forEach(data.sleeps, function(data, sleep) {
                    // misfit sleep provide a startTime an duration, we calculate Endtime, that will be the last sleepSession endTime
                    var startTime = parseInt(moment(data.startTime).format('X'));
                    var endTime = parseInt(moment(data.startTime).add(data.duration, 's').format('X'));


                    _.forEach(data.sleepDetails, function(sleepDetail, key) {
                        var nextKey = key + 1;
                        var sessionStartTime = parseInt(moment(sleepDetail.datetime).format('X'));
                        var sessionEndTime;
                        if (data.sleepDetails.hasOwnProperty(nextKey)) {

                            sessionEndTime = parseInt(moment(data.sleepDetails[nextKey].datetime).format('X'));
                        } else {
                            sessionEndTime = endTime;

                        }
                        var sessionDuration = sessionEndTime - sessionStartTime;

                        //sleepSeesion data
                        sleepSession = {
                            startTime: moment(sessionStartTime * 1000).format('LLL'),
                            endTime: moment(sessionEndTime * 1000).format('LLL'),
                            duration: sessionDuration,
                            quality: sleepDetail.value,
                            id: data.id
                        };
                        sleepSessions.push(sleepSession);
                    });
                });
                console.log(JSON.stringify(sleepSessions));

                var sleepByDay = _.groupBy(sleepSessions, function(it) {
                    return moment(it.startTime * 1000).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                });

                var sleepById = _.groupBy(sleepSessions, function(it) {
                    return it.id; //Group by day, disregarding differing times
                });



                _.forEach($scope.charts, function(data, day) {

                    var sleep = [];
                    $scope.charts[day].sleepChart = {};
                    $scope.charts[day].date = moment(day).format('LL');

                    if (sleepByDay[day]) {

                        sleepByDay[day] = _.sortBy(sleepByDay[day], 'data.startTime');


                        _.forEach(sleepByDay[day], function(data, session) {

                            sessionStart = moment(data.startTime);
                            sessionEnd = moment(data.endTime);

                            sessionStart = sessionStart.format('HH:mm');
                            sessionEnd = sessionEnd.format('HH:mm');

                            var relativeSessionStart = moment(data.startTime * 1000).diff(moment(day)) - (moment(0).utcOffset() * 60000);

                            sleep.push({
                                x: relativeSessionStart,
                                y: data.quality,
                                size: data.duration,
                                sessionStart: sessionStart,
                                sessionEnd: sessionEnd,
                                id: data.id
                            });
                        });



                        data = [{
                            values: sleep, //values - represents the array of {x,y} data points
                            key: 'sleep', //key  - the name of the series.
                            color: '#3f51b5' //color - optional: choose your own line color.
                        }];
                        $scope.charts[day].sleepChart.data = data;
                        data = [];
                    } else {
                        $scope.charts[day].sleepChart.data = [];
                    }

                });
                $scope.slideBoxUpdate('getSleep');
            }).error(function(error) {
                console.log('no connection');
            });
        }

        /**
         * [getActivity description]
         * @return {[type]}
         */
        function getActivity() {
            apiService.misfit.getSessions({
                start_date: start_date,
                end_date: end_date
            }).success(function(data) {

                var activityByDay = _.groupBy(data.sessions, function(it) {
                    return moment(it.startTime).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                });

                _.forEach($scope.charts, function(data, day) {

                    var activity = [];
                    $scope.charts[day].activityChart = {};
                    $scope.charts[day].date = moment(day).format('LL');

                    if (activityByDay[day]) {

                        activityByDay[day] = _.sortBy(activityByDay[day], 'data.startTime');


                        _.forEach(activityByDay[day], function(data, session) {

                            sessionStart = moment(data.startTime);
                            sessionEnd = moment(data.startTime).add(data.duration, 's');
                            sessionStart = sessionStart.format('HH:mm');
                            sessionEnd = sessionEnd.format('HH:mm');

                            var relativeSessionStart = moment(data.startTime).diff(moment(day)) - (moment(0).utcOffset() * 60000);


                            activity.push({
                                x: relativeSessionStart,
                                y: data.steps,
                                size: data.duration,
                                sessionStart: sessionStart,
                                sessionEnd: sessionEnd
                            });

                            data = [{
                                values: activity, //values - represents the array of {x,y} data points
                                key: 'activity', //key  - the name of the series.
                                color: '#ffc107' //color - optional: choose your own line color.
                            }];

                            $scope.charts[day].activityChart.data = data;
                            data = [];
                        });
                    } else {
                        $scope.charts[day].activityChart.data = [];
                    }

                });
                $scope.slideBoxUpdate('getActivity');
            }).error(function(error) {
                console.log('no connection');
            });
        }


        /**
         * [intakesOptions]
         * @type {Object}
         */
        $scope.intakesOptions = {
            chart: {

                margin: {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 20
                },
                height: 200,
                duration: 500,
                pointRange: [50, 300],
                reduceXTicks: false,
                type: "scatterChart",
                tooltipContent: function(key, x, y, graph) {
                    console.log(graph);
                    return '<h3>' + key.substring(0, 10) + '</h3>' + y;
                },
                color: [
                    "#1f77b4",
                    "#ff7f0e",
                    "#2ca02c",
                    "#d62728",
                    "#9467bd",
                    "#8c564b",
                    "#e377c2",
                    "#7f7f7f",
                    "#bcbd22",
                    "#17becf"
                ],
                showLegend: false,
                scatter: {
                    onlyCircles: false
                },
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 45
                },
                showYAxis: false,
                showXAxis: true,
                showValues: true,
            }
        };

        /**
         * [activityOptions description]
         * @type {Object}
         */
        $scope.activityOptions = {
            chart: {
                type: 'lineChart',

                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + graph.point.sessionStart + ' - ' + graph.point.sessionEnd + '</h3>' + y + '  steps';
                },


                margin: {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 20
                },
                duration: 500,
                reduceXTicks: false,
                interpolate: "monotone",

                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    axisLabel: 'Steps'
                },
                showYAxis: false,
                callback: function(chart) {
                    // console.log("!!! activityChart callback !!!");
                }
            }
        };

        /**
         * [sleepOptions description]
         * @type {Object}
         */
        $scope.sleepOptions = {
            chart: {
                type: 'stackedAreaChart',

                tooltipContent: function(key, x, y, graph) {
                    return '<h3>' + graph.point.sessionStart + ' - ' + graph.point.sessionEnd + '</h3>' + x + ': Level ' + y;
                },

                height: 150,
                margin: {
                    top: 0,
                    right: 20,
                    bottom: 60,
                    left: 20
                },
                duration: 500,
                reduceXTicks: false,
                interpolate: "step",
                showControls: false,
                showLegend: false,
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 50
                },
                yAxis: {
                    axisLabel: 'Sleep Quality'
                },
                showYAxis: false,
                callback: function(chart) {
                    // console.log("!!! sleepChart callback !!!");
                }
            }
        };

        /**
         * [slideBoxUpdate description]
         * @param  {[type]}
         * @return {[type]}
         */
        $scope.slideBoxUpdate = function(thefunc) {
            console.log(thefunc, moment().format('MMMM Do YYYY, h:mm:ss a'));
            if (($ionicSlideBoxDelegate.slidesCount() === 0) || ($ionicSlideBoxDelegate.currentIndex() != 6)) {
                $ionicSlideBoxDelegate.slide(6);
                console.log('The SLIDE! ', moment().format('MMMM Do YYYY, h:mm:ss a'));
            }
        };

    });
