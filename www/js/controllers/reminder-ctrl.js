angular.module('florence.controllers')

    .controller('ReminderCtrl', function($q, $scope, User, PouchDBService, sqliteService, $stateParams, Reminders, $state, $ionicPopup, NotificationService, $filter, $timeout) {


        $scope.reminder = $stateParams.reminderId;
        $scope.dosages = [0.25, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $scope.initDosagesMenu = [1];
        $scope.addButton = false;
        $scope.daySelected = 0;

        if ($stateParams.reminderId == 'add') {
            $scope.reminder = {
                "userid": User.id(),
                "hour": '12',
                "minute": '00',
                "medicines": [],
                "dayOfWeek": {
                    "1": false,
                    "2": false,
                    "3": false,
                    "4": false,
                    "5": false,
                    "6": false,
                    "7": false
                },
                "favorite": false
            };

            $scope.saveButton = true;
            $scope.showSearch = true;
            $scope.addMore = false;
            $scope.deleteShow = false;
            if ($scope.showSearch === true) {
                $timeout(function() {
                    var searchInput = document.getElementById('medication');
                    searchInput.focus();
                }, 0);
            }

        } else {
            Reminders.get($stateParams.reminderId)
                .then(function(response) {
                    $scope.reminder = response;
                    //set which days are selected
                    for (i = 1; i < 8; ++i) {
                        if ($scope.reminder.dayOfWeek[i]) {
                            $scope.daySelected = $scope.daySelected + i;
                        }
                    }
                }, function(err) {
                    $state.go("tab.deck");
                });
            $scope.saveButton = true;
            $scope.showSearch = false;
            $scope.addMore = true;
            $scope.deleteShow = true;
        }

        $scope.showDosage = function() {
            $scope.dosagesMenu = $scope.dosages;
            $scope.initDosagesMenu = [];
            $scope.showMedicationPreview = false;
        };

        $scope.selectDosage = function(selected) {
            $scope.medicine.dosage = selected;
            $scope.dosagesMenu = [];
            $scope.initDosagesMenu = [selected];
            $scope.showMedicationPreview = false;
            $timeout(function() {
                $scope.addButton = true;
            }, 500);
        };

        $scope.selectedMedicine = function(selected) {
            $scope.medicine = selected.originalObject;
            $scope.medicine.dosage = 1;
            $scope.initDosagesMenu = [1];
            $scope.showMedicationPreview = false;
        };

        $scope.addMedicine = function() {
            var newMedicine = {};
            newMedicine = $scope.medicine;
            $scope.reminder.medicines.push($scope.medicine);
            $scope.medicine = null;
            $scope.initDosagesMenu = [1];
            $scope.addButton = false;
            $scope.saveCardButton();
            $scope.showSearch = false;
            $timeout(function() {
                $scope.addMore = true;
            }, 500);
        };

        $scope.addReminder = function() {
            Reminders.addReminder($scope.reminder);
            $state.go("tab.deck");
        };

        $scope.deleteReminder = function() {
            Reminders.deleteReminder($scope.reminder);
            $state.go("tab.deck");
        };

        $scope.addAnother = function() {
            $scope.showSearch = true;
            $scope.addMore = false;
        };

        $scope.saveCardButton = function() {
            if ($scope.reminder.medicines.length > 0 && $scope.daySelected > 0) {
                if ($scope.saveButton) {
                    $scope.saveButton = !$scope.saveButton;
                }
            } else {
                $scope.saveButton = true;
            }
        };

        $scope.selectDay = function(day, enabled) {
            if (day == 'all') {
                $scope.reminder.dayOfWeek = {

                    "1": enabled,
                    "2": enabled,
                    "3": enabled,
                    "4": enabled,
                    "5": enabled,
                    "6": enabled,
                    "7": enabled
                };
                if (enabled) {
                    $scope.daySelected = 28;
                } else {
                    $scope.daySelected = 0;
                }
            } else {
                if (enabled) {
                    $scope.daySelected = $scope.daySelected + parseInt(day);
                } else {
                    $scope.daySelected = $scope.daySelected - parseInt(day);
                    $scope.allDays = false;
                }
            }
            $scope.saveCardButton();
        };

        $scope.blankReplace = function(type) {
            if (type == 'hour') {
                if ($scope.reminder.hour === '') {
                    $scope.reminder.hour = $scope.tempHour;
                }
                if ($scope.reminder.hour.length < 2) {
                    $scope.reminder.hour = '0' + $scope.reminder.hour;
                }
            }
            if (type == 'minute') {
                if ($scope.reminder.minute === '') {
                    $scope.reminder.minute = $scope.tempMinute;
                }
                if ($scope.reminder.minute.length < 2) {
                    $scope.reminder.minute = '0' + $scope.reminder.minute;
                }
            }
        };

        $scope.clearField = function(type) {

            if (type == 'hour') {
                $scope.tempHour = $scope.reminder.hour;
                $scope.reminder.hour = '';
            }
            if (type == 'minute') {
                $scope.tempMinute = $scope.reminder.minute;
                $scope.reminder.minute = '';
            }
        };

        $scope.changeTime = function(type) {
            if (type == 'hour') {
                $scope.reminder.hour = $scope.reminder.hour.replace(/\D+/g, '').substring(0, 2);
                if (parseInt($scope.reminder.hour) > 23) {
                    $scope.reminder.hour = 23;
                }

            }
            if (type == 'minute') {
                $scope.reminder.minute = $scope.reminder.minute.replace(/\D+/g, '').substring(0, 2);
                if (parseInt($scope.reminder.minute) > 59) {
                    $scope.reminder.minute = 59;
                }

            }
            $scope.saveCardButton();
        };

        $scope.deleteConfirmPopUp = function() {

            var header = $filter('translate')('DELETE_CARD');
            var body = $filter('translate')('DELETE_CARD_CONFIRM');
            var button_cancel = $filter('translate')('CANCEL');
            var button_delete = $filter('translate')('DELETE');

            deletePopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive popup-Buttons',
                    text: button_cancel,
                    onTap: function(e) {
                        deletePopup.close();
                    }
                }, {
                    type: 'button button-clear button-balanced popup-Buttons',
                    text: button_delete,
                    onTap: function(e) {
                        $scope.deleteReminder();
                        deletePopup.close();
                    }
                }]
            });
            deletePopup.then(function(res) {});
        };
    });
