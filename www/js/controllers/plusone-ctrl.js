angular.module('florence.controllers')

    .controller('PlusoneCtrl', function($scope, Intakes, $ionicPopup, Plusones, sqliteService, $stateParams, $state, $filter) {

        $scope.mood = {};
        $scope.pain = {};
        $scope.pain.types = ["DULL", "BURNING", "SCURRYING", "INCISIVE", "PULSATING", "PUNGENT", "SWELTERING", "OTHER_PAIN"];
        $scope.pain.typeItems = [];
        $scope.pain.intake = {};
        $scope.note = {};
        $scope.dosages = [0.25, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $scope.Ndosages = [{
            val: 0.25,
            sel: false
        }, {
            val: 0.5,
            sel: false
        }, {
            val: 0.75,
            sel: false
        }, {
            val: 1,
            sel: true
        }, {
            val: 2,
            sel: false
        }, {
            val: 3,
            sel: false
        }, {
            val: 4,
            sel: false
        }, {
            val: 5,
            sel: false
        }, {
            val: 6,
            sel: false
        }, {
            val: 7,
            sel: false
        }, {
            val: 8,
            sel: false
        }, {
            val: 9,
            sel: false
        }];


        if ($stateParams.plusoneId) {
            Plusones.get($stateParams.plusoneId)
                .then(function(response) {
                    $scope.plusone = response;

                    $scope.dosage = $scope.plusone.dosage;
                    $scope.initDosagesMenu = [$scope.dosage];
                }, function(err) {
                    console.log(err);
                    $state.go("tab.deck");
                });
        }

        $scope.removePlusOne = function() {
            $scope.plusone = [];
            $scope.plusonePrev = false;
        };
        $scope.selectedMedicine = function(selected) {
            $scope.plusonePrev = true;
            $scope.plusone = selected.originalObject;
            $scope.plusone.dosage = 1;
            $scope.initDosagesMenu = [1];
        };
        $scope.setDose = function(Ndose) {
            _.each($scope.Ndosages, function(dose) {
                if (dose.val !== Ndose.val) {
                    dose.sel = false;
                }
                $scope.dosage = Ndose.val;
            });
        };
        $scope.addMood = function() {
            Intakes.addMoodIntake($scope.mood.value);
            $state.go("tab.deck");
        };
        $scope.showPainType = function() {
            $scope.pain.typeItems = $scope.pain.types;
        };
        $scope.selectPainType = function(selected) {
            $scope.pain.intake.type = selected;
            $scope.pain.typeItems = [];
        };
        $scope.addPain = function() {
            Intakes.addPainIntake($scope.pain.intake);
            $state.go("tab.deck");
        };
        $scope.onNoteChange = function() {};
        $scope.addNote = function() {
            Intakes.addNoteIntake($scope.note.value);
            $state.go("tab.deck");
        };
        $scope.selectPlusOneDosage = function(selected) {
            $scope.dosage = selected;
            $scope.initDosagesMenu = [$scope.dosage];
            $scope.dosagesMenu = [];
        };
        $scope.showDosage = function() {
            $scope.dosagesMenu = $scope.dosages;
            $scope.initDosagesMenu = [$scope.dosage];
        };
        $scope.addIntake = function() {
            Intakes.addPlusoneIntake($scope.plusone, $stateParams.plusoneId, $scope.dosage);
            $state.go("tab.deck");
        };
        $scope.selectDosage = function(selected) {
            $scope.plusone.dosage = selected;
            $scope.dosagesMenu = [];
            $scope.initDosagesMenu = [selected];
        };
        $scope.addPlusOne = function() {
            Plusones.add($scope.plusone).then(function() {
                $state.go("tab.deck");
            });
        };
        $scope.deletePlusOne = function() {
            Plusones.delete($scope.plusone).then(function() {
                $state.go("tab.deck");
            });
        };
        $scope.deleteConfirmPopUp = function() {
            var header = $filter('translate')('DELETE_CARD');
            var body = $filter('translate')('DELETE_CARD_CONFIRM');
            var button1 = $filter('translate')('CANCEL');
            var button2 = $filter('translate')('DELETE');

            deletePopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive popup-buttons',
                    text: button1,
                    onTap: function(e) {
                        console.log('card not deleted');
                        deletePopup.close();
                    }
                }, {
                    type: 'button button-clear button-balanced popup-buttons',
                    text: button2,
                    onTap: function(e) {
                        $scope.deletePlusOne();
                        console.log('card deleted');
                        deletePopup.close();
                    }
                }]
            });
            deletePopup.then(function(res) {
                console.log('done');
            });
        };

        $scope.mils = 25;
        $scope.grams = 100;
        $scope.showFood = true;

        $scope.Drinks = [{
                name: 'Tea',
                cal: 8,
                sel: false
            }, {
                name: 'Coffee',
                cal: 70,
                sel: false
            }, {
                name: 'Water',
                cal: 1,
                sel: false
            }, {
                name: 'Juice',
                cal: 44,
                sel: false
            }, {
                name: 'Milk',
                cal: 1,
                sel: false
            }, {
                name: 'Soda',
                cal: 43,
                sel: false
            }, {
                name: 'Beer',
                cal: 32,
                sel: false
            }, {
                name: 'Wine',
                cal: 74,
                sel: false
            }, {
                name: 'Liquor',
                cal: 222,
                sel: false
            }

        ];

        $scope.graphdata = [{
            key: "Protein",
            y: 90,
            color: '#F44336',
            radius: 50
        }, {
            key: "Carbs",
            y: 60,
            color: '#8BC34A',
            radius: 39
        }, {
            key: "Fat",
            y: 30,
            color: '#03A9F4',
            radius: 28
        }];

        $scope.options = {
            chart: {
                type: 'pieChart',
                height: 200,
                color: [
                    "#1f77b4",
                    "#ff7f0e",
                    "#2ca02c"
                ],
                x: function(d) {
                    return d.key;
                },
                y: function(d) {
                    return d.y;
                },
                showLabels: false,
                showLegend: true,
                transitionDuration: 500,
                labelThreshold: 0.01

            }
        };

        $scope.offAll = function() {
            for (i = 0; i < $scope.Drinks.length; i++) {
                $scope.Drinks[i].sel = false;
            }
        };

        $scope.back = function() {
            $rootScope.$broadcast('showFrontPanel');
            $scope.initIntakes();
        };
    });
