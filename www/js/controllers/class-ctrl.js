angular.module('florence.controllers')

    .controller('ClassCtrl', ['$scope', 'apiService', '$stateParams', '$sce', 'localStorageService', function($scope, apiService, $stateParams, $sce, localStorageService) {
        apiService.class.show($stateParams.classId).success(function(data) {
            $scope.class = data;
        }).error(function() {
            var classes = localStorageService.get('classes');
            $scope.class = _.find(classes, {
                id: parseInt($stateParams.classId)
            });
        });

        $scope.trustAsHtml = function(string) {
            return $sce.trustAsHtml(string);
        };
    }]);
