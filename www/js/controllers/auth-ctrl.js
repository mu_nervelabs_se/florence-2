angular.module('florence.controllers')

    .controller('AuthCtrl', function($scope, User, apiService, $state, $translate, $animate, PouchDBService, $rootScope, $timeout) {

        User.get().then(function() {
            console.log('User.get in AuthCtrl triggered --------------------');
            console.log('state.go tab.deck in AuthCtrl triggered --------------------');
            $state.go('tab.deck');

        });
        $scope.reset = {};
        $scope.loginForm = {};

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                if (toState.name == "tab.auth-login") {
                    $scope.loginForm = {};
                }
            }
        );

        $scope.login = function() {
            $scope.loginErr = false;
            User.login($scope.loginForm.username, $scope.loginForm.password)
                .then(function(success) {
                    //$scope.user = User.get();
                    //console.log('Call User.get in AuthCtrl -------------------------------');

                    console.log('Call window.location.reload(true) timeout 2000 begin -------------------------------');
                    $timeout(function() {
                        window.location.reload(true);
                    }, 2000);
                    console.log('Call window.location.reload(true) timeout 2000 end   -------------------------------');

                }, function(error) {
                    $scope.message = 'HTTP_' + error;
                    $scope.loginErr = true;
                });
        };

        $scope.openTermsUrl = function() {
            $translate('TERMS_URL').then(function(url) {
                window.open(url, '_system');
            });
        };

        $scope.openPrivacyPolicyUrl = function() {
            $translate('PRIVACY_POLICY_URL').then(function(url) {
                window.open(url, '_system');
            });
        };


        $scope.sendPasswordResetMail = function(email) {
            $scope.remailErr = false;
            apiService.user.sendPasswordResetMail(email)
                .success(function(data) {
                    $scope.error = null;
                    $scope.success = true;
                }).error(function(data, status) {
                    $scope.error = 'HTTP_' + status;
                    $scope.remailErr = true;
                });
        };

        $scope.signup = function() {

            $scope.signupErr = false;
            $scope.success = {};
            $scope.error = {};

            var data = {
                username: this.username,
                email: this.email,
                password: this.password
            };

            apiService.user.register(data)

                .success(function(data, status) {
                    if (data.error) {
                        $scope.message = data.message;
                    } else {
                        $scope.user = User.get().then(function() {
                            PouchDBService.initPouchDbs();
                            console.log('initPouchDbs executed ---------------');
                            $rootScope.$broadcast('initPouchForUser');
                            console.log('initPouchForUser broadcasted ---------------');
                            $state.go('tab.deck');
                            console.log('state.go tab.deck ---------------');
                        });
                    }
                })
                .error(function(data, status) {
                    $scope.message = 'HTTP_' + status;
                    $scope.signupErr = true;
                });
        };


        $scope.ping = function() {
            $scope.success = {};
            $scope.error = {};

            //User.ping()
            //  .then(function(success) {
            //    $scope.success.message = success._id;
            //  }, function(error) {
            //    $scope.error = error.data.error;
            //  });
        };
    });
