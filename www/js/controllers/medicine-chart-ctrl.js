angular.module('florence.controllers')

    .controller('MedicineChartCtrl', function($scope, Intakes, apiService, $state, $ionicSlideBoxDelegate, $timeout, $ionicPopup, $filter) {

        console.log('MedicineChartCtrl START', moment().format('MMMM Do YYYY, h:mm:ss a'));
        //Time range: last 7 days.
        var start_date = moment().subtract(6, 'days').format('YYYY-MM-DD');
        var end_date = moment().subtract(0, 'days').format('YYYY-MM-DD');

        //We prefilled with days
        $scope.charts = {};
        for (var i = 6; i >= 0; i--) {
            var day = moment().subtract(i, 'days').format('YYYY-MM-DD');
            $scope.charts[day] = {};
        }

        moment.locale('en', {
            calendar: {
                lastDay: '[yesterday]',
                sameDay: '[today]',
                nextDay: '[tomorrow]',
                lastWeek: 'dddd',
                nextWeek: 'dddd',
                sameElse: 'L'
            }
        });
        moment.locale('sv', {
            calendar: {
                lastDay: '[igår]',
                sameDay: '[idag]',
                nextDay: '[imorgon]',
                lastWeek: 'dddd',
                nextWeek: 'dddd',
                sameElse: 'L'
            }
        });
        /**
         * [getIntakes description]
         * @return {[type]}
         */
        $scope.getIntakes = function() {

            Intakes.getIntakes().then(function(response) {

                    var intakesByDay = _.groupBy(response, function(it) {
                        return moment(it.doc.TS * 1000).format('YYYY-MM-DD'); //Group by day, disregarding differing times
                    });
                    var adhearData = [];
                    var takenOT = [];
                    var takenTL = [];
                    var takenNT = [];


                    intakesByDay[day] = _.sortBy(intakesByDay[day], 'doc.TS');


                    _.forEach($scope.charts, function(empty, day) {
                        var reminderCount = 0;
                        var OTscore = 0;
                        var TLscore = 0;
                        var NTscore = 0;


                        $scope.charts[day].intakesChart = {};
                        $scope.charts[day].date = moment(day).format('LL');

                        if (intakesByDay[day]) {

                            var data = [];
                            var newData = [];


                            //series based on nplid
                            var intakesByNplid = _.groupBy(intakesByDay[day], function(it) {
                                return it.doc.tradename; //Group by nplid
                            });
                            var i = 0;
                            _.forEach(intakesByNplid, function(tradenameIntakes, tradename) {
                                var values = [];
                                i++;
                                _.forEach(tradenameIntakes, function(intake, session) {
                                    //var relativeSessionStart = moment(intake.doc.TS * 1000).diff(moment(day)) - (moment(0).utcOffset() * 60000);
                                    var relativeSessionStart = moment.unix(intake.doc.TS);

                                    //alert(JSON.stringify(intake.doc.kind + ' ' + intake.doc.takenTime + ' ' + intake.doc.reminderTime));

                                    if (intake.doc.takenHour) {
                                        relativeSessionStart = moment(relativeSessionStart).set({
                                            'hour': intake.doc.takenHour
                                        })
                                    };
                                    if (intake.doc.takenHour) {
                                        relativeSessionStart = moment(relativeSessionStart).set({
                                            'minute': intake.doc.takenMinute
                                        })
                                    };


                                    if (intake.doc.kind == 'Reminder') {
                                        if (intake.doc.takenTime == intake.doc.reminderTime && intake.doc.checked) {
                                            ++OTscore
                                        };
                                        if (intake.doc.takenTime != intake.doc.reminderTime && intake.doc.checked) {
                                            ++TLscore
                                        };
                                        if (!intake.doc.checked) {
                                            NTscore = ++NTscore
                                        };
                                        reminderCount = reminderCount + 1;

                                    }

                                    values.push({
                                        x: relativeSessionStart,
                                        y: intake.doc.dosage,
                                    });
                                });


                                data.push({
                                    values: values, //values - represents the array of {x,y} data points
                                    key: tradename //key  - the name of the series.
                                });

                            });
                            takenOT.push({
                                x: moment(day),
                                y: OTscore,

                            });

                            takenTL.push({
                                x: moment(day),
                                y: TLscore,

                            });

                            takenNT.push({
                                x: moment(day),
                                y: NTscore,

                            });

                            //alert(JSON.stringify(reminderCount));
                            var OTLabel = $filter('translate')('ON_TIME');
                            var TLLabel = $filter('translate')('TAKEN_LATER');
                            var NTLabel = $filter('translate')('NOT_TAKEN');

                            adhearData = [{
                                values: takenOT, //values - represents the array of {x,y} data points
                                key: OTLabel,
                                color: '#4CAF50' //key  - the name of the series.
                            }, {
                                values: takenTL, //values - represents the array of {x,y} data points
                                key: TLLabel,
                                color: '#C8E6C9' //key  - the name of the series.
                            }, {
                                values: takenNT, //values - represents the array of {x,y} data points
                                key: NTLabel,
                                color: '#FF5722' //key  - the name of the series.
                            }];
                            //console.log(JSON.stringify(adhearData));

                            $scope.charts[day].intakesChart.data = data;
                        } else {
                            $scope.charts[day].intakesChart.data = [];
                        }

                        $scope.adhearanceChart = adhearData;
                        //alert(JSON.stringify($scope.adhearanceChart));
                    });
                    $scope.slideBoxUpdate('intakes');
                },
                function(err) {
                    console.log(err);
                });
        }

        $scope.getIntakes();


        /**
         * [intakesOptions]
         * @type {Object}
         */
        $scope.intakesOptions = {
            chart: {

                margin: {
                    top: 0,
                    right: 20,
                    bottom: 40,
                    left: 20
                },
                height: 220,
                duration: 500,
                type: "multiBarChart",
                tooltipContent: function(d) {
                    console.log(graph);
                    return '<h3>' + d.key.substring(0, 8) + '</h3>' + d.y;
                },
                color: [
                    "#1f77b4",
                    "#ff7f0e",
                    "#2ca02c",
                    "#d62728",
                    "#9467bd",
                    "#8c564b",
                    "#e377c2",
                    "#7f7f7f",
                    "#bcbd22",
                    "#17becf"
                ],
                legend: {
                    key: function(d) {
                        return d.key.substring(0, 8);
                    },
                    margin: {
                        top: 5,
                        right: 0,
                        bottom: 15,
                        left: 0
                    },
                    width: 400,
                    align: true,
                    rightAlign: false
                },
                xAxis: {
                    tickFormat: function(d) {
                        return moment(d).format('HH:mm');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    axisLabel: 'Dosage'
                },
                groupSpacing: 0.2,
                showYAxis: true,
                showXAxis: true,
                showValues: true,
                showLegend: true,
                showControls: false,
                reduceXTicks: false,
                staggerLabels: false,
                rightAlignYAxis: true
            }
        };


        $scope.adhearOptions = {
            chart: {

                margin: {
                    top: 0,
                    right: 20,
                    bottom: 40,
                    left: 20
                },
                height: 200,
                duration: 500,
                reduceXTicks: false,
                type: "multiBarChart",
                multibar: {
                    stacked: true
                },
                interpolate: 'basis',
                tooltipContent: function(key, x, y, graph) {
                    console.log(graph);
                    return '<h3>' + key.substring(0, 10) + '</h3>' + y;
                },
                xAxis: {
                    tickFormat: function(key) {
                        return moment(key).format('dd');
                    },
                    rotateLabels: 45
                },
                yAxis: {
                    tickFormat: function(y) {
                        return y
                    }
                },
                legend: {
                    margin: {
                        top: 5,
                        right: 0,
                        bottom: 15,
                        left: 0
                    },
                    width: 400,
                    align: true,
                    rightAlign: false
                },
                groupSpacing: 0.5,
                showYAxis: true,
                showXAxis: true,
                showValues: true,
                showLegend: true,
                showControls: false,
                rightAlignYAxis: true
            }
        };


        $scope.adhearGraphPopup = function() {

            var header = $filter('translate')('Utfallsdiagram över påminnelser');
            var body = '<p>Det här diagrammet är till för de som måste följa ett strikt schema för medicinering. Diagrammet kan användas för att optimera ditt schema.<br><br> Diagrammet visar när du tog dina mediciner enligt påminnelserna, när du tog dem senare och när du inte tog dem. Det här diagrammet kan vara till hjälp när du behöver förbättra ditt medicinschema för att nå ett bättre resultat.</p>';
            var button1 = $filter('translate')('CLOSE');

            adhearPopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
                    onTap: function(e) {
                        console.log('card closed');
                        adhearPopup.close();
                    }
                }]
            });
            adhearPopup.then(function(res) {
                console.log('done');
            });
        };

        $scope.medicationGraphPopup = function() {

            var header = $filter('translate')('Medicin diagram');
            var body = '<p>Det här diagrammet visar dig när du mar missat att ta din medicin. Olika färger symboliserar olika mediciner. I menyn kan slå på eller av olika värden om du vill fokusera mer specifikt. Detta gör du genom att tappa på meny knappen för att växla mellan av och på.</p>';
            var button1 = $filter('translate')('CLOSE');

            medicationPopup = $ionicPopup.show({
                title: header,
                template: body,
                scope: $scope,
                buttons: [{
                    type: 'button button-clear button-assertive profilePopButton ion-ios-close-empty',
                    onTap: function(e) {
                        console.log('card closed');
                        medicationPopup.close();
                    }
                }]
            });
            medicationPopup.then(function(res) {
                console.log('done');
            });
        };


        /**
         * [slideBoxUpdate description]
         * @param  {[type]}
         * @return {[type]}
         */
        $scope.slideBoxUpdate = function(thefunc) {
            console.log(thefunc, moment().format('MMMM Do YYYY, h:mm:ss a'));
            if (($ionicSlideBoxDelegate.slidesCount() === 0) || ($ionicSlideBoxDelegate.currentIndex() != 6)) {
                $ionicSlideBoxDelegate.slide(6);
                console.log('The SLIDE! ', moment().format('MMMM Do YYYY, h:mm:ss a'));
            }
        };

    });
