angular.module('florence.controllers')
    .controller('ContactCtrl', function($scope, $rootScope) {

        $scope.version = $rootScope.version;
        $scope.build = $rootScope.build;
        var deviceInformation = ionic.Platform.device();
        deviceInformation.florence_version = $rootScope.version;
        deviceInformation.florence_build = $rootScope.build;
        var deviceInformationBody = angular.toJson(deviceInformation, true);

        if (window.cordova) {
            $scope.sendFeedback = function() {
                cordova.plugins.email.open({
                    to: 'florence@nervelabs.se',
                    subject: 'Feedback for Florence',
                    body: 'Device Information:\r' + deviceInformationBody,
                }, function(result) {
                    console.log("Response -> " + result);
                });
            };
        }
    });
