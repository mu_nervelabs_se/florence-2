angular.module('florence.services')

    .factory('Intakes', function($rootScope, $q, PouchDBService) {

        var intakesDB = PouchDBService.getIntakesDB();

        $rootScope.$on('initPouchForUser', function() {
            intakesDB = PouchDBService.getIntakesDB();
        });

        return {

            addBasicIntakeData: function(intake, notification) {
                var rightNow = moment();

                intake.userid = notification.userid;
                intake.reminderId = notification.data.id;

                //We should reduce the verbosity
                intake.TS = rightNow.format('X');
                intake.takenTime = rightNow.format('HH:mm');
                intake.takenHour = rightNow.format('HH');
                intake.takenMinute = rightNow.format('mm');
                intake.date = rightNow.format();
                intake.dateCreated = new Date();

                intake.reminderTime = notification.data.hour + ':' + notification.data.minute;
                intake.hour = intake.hour;
                intake.minute = notification.data.minute;

                intake.kind = 'Reminder';
            },

            addBasicIntakeDataFromReminder: function(intake, reminder, date) {
                date = moment(date);
                intake.userid = reminder.userid;
                intake.reminderId = reminder._id;

                //We should reduce the verbosity
                intake.TS = date.format('X');
                intake.takenTime = reminder.hour + ':' + reminder.minute;
                intake.takenHour = reminder.hour;
                intake.takenMinute = reminder.minute;
                intake.date = date.toDate();
                intake.dateCreated = new Date();

                intake.reminderTime = reminder.hour + ':' + reminder.minute;
                intake.hour = reminder.hour;
                intake.minute = reminder.minute;

                intake.kind = 'Reminder';
            },


            addPlusoneIntake: function(plusone, plusoneId, dosage) {
                var currentDate = moment();
                var intake = {};

                //We create a copy of the plusone card data
                angular.copy(plusone, intake);

                //We delete irrelevant info
                delete intake._id;
                delete intake._rev;

                //We populate with the intake input
                intake.dosage = dosage;
                intake.reminderId = plusoneId;
                intake.TS = currentDate.format('X');
                intake.takenTime = currentDate.format('HH:mm');
                intake.date = currentDate.format();
                intake.reminderTime = intake.takenTime;
                intake.hour = currentDate.format('HH');
                intake.minute = currentDate.format('mm');
                intake.checked = true;
                intake.kind = 'Plusone';
                this.addIntake(intake);
            },

            updateOutdatedIntakeData: function(it) {
                if (!it.doc.takenHour) {
                    if (!it.doc.takenTime) {
                        it.doc.takenTime = moment(it.doc.TS * 1000).format('HH:mm');
                    }

                    var takenTimeSplit = it.doc.takenTime.split(':');

                    it.doc.takenHour = takenTimeSplit[0];
                    it.doc.takenMinute = takenTimeSplit[1];

                    this.updateIntake(it).then(function(response) {
                        it.doc._rev = response.rev;
                    });
                }


                if (!it.doc.kind && it.doc.type) {
                    it.doc.kind = it.doc.type;

                    this.updateIntake(it).then(function(response) {
                        it.doc._rev = response.rev;
                    });
                }


                if (it.doc.kind === "mood") {
                    it.doc.kind = 'Mood';

                    this.updateIntake(it).then(function(response) {
                        it.doc._rev = response.rev;
                    });
                }
            },

            addMoodIntake: function(moodValue) {
                var intake = {};
                intake.TS = parseInt(moment().format('X'));
                intake.value = moodValue;
                intake.kind = 'Mood';
                this.addIntake(intake);
            },

            addPainIntake: function(pain) {
                var intake = {};
                intake.TS = parseInt(moment().format('X'));
                intake.value = pain.value;
                intake.painType = pain.type;
                intake.kind = 'Pain';
                this.addIntake(intake);
            },

            addNoteIntake: function(noteValue) {
                var intake = {};
                intake.TS = parseInt(moment().format('X'));
                intake.value = noteValue;
                intake.kind = 'Note';
                this.addIntake(intake);
            },

            getIntakes: function() {
                console.log('getIntakes in intake service triggered -------------');
                return intakesDB.allDocs({
                    include_docs: true
                }).then(function(response) {
                    intakes = response.rows;
                    return intakes;
                    console.log('.then in getIntakes in intake service triggered -------------');
                });
            },

            getRecent: function(hours, intakeKind) {
                console.log('getRecent in intake service triggered -------------');
                return intakesDB.allDocs({
                    include_docs: true
                }).then(function(response) {
                    console.log('.then in getRecent in intake service triggered -------------');
                    intakes = response.rows;
                    var newIntakes = [];

                    // the timestamp after which we want to look for intakes
                    var startTS = moment().subtract(hours, 'hours').format('X');

                    for (var i = 0; i < intakes.length; i++) {
                        //if the timestamp of the intake is older and is the kind we look after
                        if ((intakes[i].doc.TS > startTS) && (intakes[i].doc.kind == intakeKind)) {
                            newIntakes.push(intakes[i]);
                        }
                    }
                    return newIntakes;
                })
            },

            deleteIntakes: function() {
                return intakesDB.allDocs({
                    include_docs: true,
                }).then(function(result) {
                    for (var i = 0; i < result.rows.length; i++) {
                        intakesDB.remove(result.rows[i].doc._id, result.rows[i].doc._rev, function(err, res) {});
                    }
                    return result.rows;
                }).catch(function(err) {
                    console.log(angular.toJson(err));
                });
            },


            deleteIntake: function(intake) {
                var deferred = $q.defer();
                intakesDB.remove(intake.doc._id, intake.doc._rev, function(err, res) {
                    deferred.resolve();
                });
                return deferred.promise;
            },

            addIntake: function(intake) {
                var deffered = $q.defer();
                //Create a new intake and let PouchDB generate an _id for it.
                intakesDB.post(intake).then(function(response) {
                    deffered.resolve(response.id);
                }, function(err) {
                    console.log(err);
                });
                return deffered.promise;
            },

            updateIntake: function(intake) {
                var deffered = $q.defer();
                intakesDB.get(intake.id).then(function(originalDoc) {
                    intakesDB.put(intake.doc).then(function(response) {
                        deffered.resolve(response);
                    }, function(err) {
                        console.log(err);
                    });
                });
                return deffered.promise;
            },
        }
    });
