angular.module('florence.services')

  .factory('sqliteService', function($rootScope) {

    if (window.cordova) {
      window.plugins.sqlDB.copy("nerve_medicines_38.db", 2, copysuccess, copyerror);
      console.log("window.plugins.sqlDB.copy executed!!!");

      function copysuccess() {
        //open db and run your queries
        console.log("copy success");
      }

      function copyerror(e) {
        //db already exists or problem in copying the db file. Check the Log.
        console.log("Error Code = " + JSON.stringify(e));
        //e.code = 516 => if db exists
      }
      db = window.sqlitePlugin.openDatabase({
        name: "nerve_medicines_38.db",
        location: 'default'
      }, function(s) {
        console.log('success', angular.toJson(s, true))
      }, function(e) {
        console.log(angular.toJson(e, true))
      });
      // MySql.Execute(
      //     "37.123.161.66",
      //     "florence",
      //     "florence",
      //     "medicine",
      //     "select * from se",
      //     function(data) {
      //         console.log(data)
      //     });


    } else {
      console.error('Works only on device/simulator')
    }

    /*
    Workaround to get medicines db works on browser: broken since US med
     */
    return {
      openDB: function(dbFile) {
        var deffered = $q.defer();
        //The window.openDatabase creates a new SQL Lite Database and returns a Database object that allows manipulation of the data.
        db = window.openDatabase(dbFile, "1.0", "SQL Lite DB", 1024 * 1024);
        deffered.resolve(db);
        db.transaction(function(tx) {
          tx.executeSql('CREATE TABLE IF NOT EXISTS medicines (nplid ,tradename ,strength_group_name ,pharmaceutical_form_group_name ,organization  )');

        });
        db.transaction(function(tx) {
          tx.executeSql("SELECT * FROM medicines", [], function(tx, results) {

            if (results.rows.length <= 0) {
              $http.get('/nerve_medicines.sql').success(function(data, status, headers, config) {
                console.log('------------>', 'import nerve_medicines.sql file to sqlLite');
                processQuery(db, 2, data.split(';\n'));
              }).error(function(error) {});
            }
          });
        });
        return deffered.promise;
      },
      dbVersion: $rootScope.version
    };

    function processQuery(db, i, queries) {
      if (i < queries.length - 1) {
        //console.log(i + ' of ' + queries.length);
        if (!queries[i + 1].match(/(INSERT|CREATE|DROP|PRAGMA|BEGIN|COMMIT)/)) {
          queries[i + 1] = queries[i] + ';\n' + queries[i + 1];
          return processQuery(db, i + 1, queries);
        }
        //console.log('------------>', queries[i]);
        db.transaction(function(query) {
          query.executeSql(queries[i] + ';', [], function(tx, result) {
            processQuery(db, i + 1, queries);
          });
        }, function(err) {
          console.log("Query error in ", queries[i], err.message);
          processQuery(db, i + 1, queries);
        });
      } else {
        console.log('------------>', 'nerve_medicines.sql imported in sqlLite');
      }
    }
  });
