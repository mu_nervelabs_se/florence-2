angular.module('florence.services')

    .factory('Plusones', function($rootScope, $q, User, PouchDBService) {
        var plusones = {};
        var plusonesDB = PouchDBService.getPlusoneDB();

        $rootScope.$on('initPouchForUser', function() {
            plusonesDB = PouchDBService.getPlusoneDB();
        });

        return {
            getAll: function(force) {
                var deferred = $q.defer();
                if (_.isEmpty(plusones) || force === true) {
                    plusonesDB.allDocs({
                        include_docs: true
                    }).then(function(response) {
                        plusones = _.map(response.rows, function(plusone) {
                            return plusone.doc;
                        });
                        deferred.resolve(plusones);
                    });
                } else {
                    deferred.resolve(plusones);
                }
                return deferred.promise;
            },

            get: function(id) {
                var deffered = $q.defer();
                for (i = 0; i < plusones.length; i++) {
                    if (plusones[i]._id == id) {
                        deffered.resolve(plusones[i]);
                        return deffered.promise;
                    }
                }
                deffered.reject('no result');
                return deffered.promise;
            },

            add: function(plusone) {
                plusone.userid = User.id();
                var deffered = $q.defer();
                //Create a new plusone and let PouchDB generate an _id for it.
                plusonesDB.post(plusone).then(function(response) {
                    console.log(angular.toJson(response, true));
                    plusone._id = response.id;
                    plusone._rev = response.rev;
                    plusones.push(plusone);
                    deffered.resolve(response.id);
                }, function(err) {
                    console.log(err);
                });
                return deffered.promise;
            },

            delete: function(plusone) {
                var deffered = $q.defer();
                //Cache Update
                for (i = 0; i < plusones.length; i++) {
                    if (plusones[i]._id == plusone._id) {
                        plusones.splice(i);
                    }
                }
                plusonesDB.remove(plusone._id, plusone._rev).then(function(response) {
                    deffered.resolve(response);
                }, function(err) {
                    console.log(err);
                });
                return deffered.promise;
            },
        };
    });
