angular.module('florence.services')

    .factory('PouchDBService', function($rootScope, localStorageService, NotificationService, $timeout) {

        console.log('factory.PouchDBService begin ---------------------------------')
        var databases = ['reminders', 'intakes', 'plusones', 'fingertaps', 'profile'];
        var syncActive = false;
        //var remoteCouchDbUrl = 'http://95.109.5.38:5984';
        //var remoteCouchDbUrl = 'http://localhost:5984';
        var remoteCouchDbUrl = 'https://api.nervelabs.se/couchdb';

        var pouchDBs = {};
        var activeSyncs = {};

        var initPouchDbs = function() {
            console.log('initPouchDbs begin -----------------------------');
            var currentUser = localStorageService.get('user');
            if (!currentUser || !currentUser.id) {
                console.log('initPouchDbs, !currentUser || !currentUser.id = false, initPouchDbs return !');
                return;
            }
            _.forEach(databases, function(dbName) {
                pouchDBs[dbName] = new PouchDB('user_' + currentUser.id + '_' + dbName);
                console.log('Database  ' + dbName + '  inited----------------');
                console.log('Database  ' + dbName + 'is using adapter ' + dbName.adapter);
                pouchDBs[dbName].info().then(console.log.bind(console));

                if (dbName == 'profile') {
                    pouchDBs[dbName].get('profile').then(
                        function() {},
                        function() {
                            pouchDBs[dbName].put({
                                _id: 'profile'
                            });
                        });
                }
            });
            console.log('initPouchDbs end -----------------------------');
        };

        $rootScope.$on('initPouchForUser', function() {
            console.log('on.initPouchForUser begin --------------------------------');
            initPouchDbs();
            console.log('on.initPouchForUser end-----------------------------------');
        });
        console.log('initPouchDbs in factory.PouchDBService begin --------------------------');
        initPouchDbs();
        console.log('initPouchDbs in factory.PouchDBService end   --------------------------');


        console.log('factory.PouchDBService end ---------------------------------');

        console.log('factory.PouchDBService.return section triggerd ---------------------------------');
        return {

            initPouchDbs: initPouchDbs,

            getReminderDB: function() {
                console.log('getReminderDB triggerd in return section in initPouchDbs');
                return pouchDBs.reminders;
            },
            getIntakesDB: function() {
                console.log('getIntakesDB triggerd in return section in initPouchDbs');
                return pouchDBs.intakes;
            },
            getPlusoneDB: function() {
                console.log('getPlusoneDB triggerd in return section in initPouchDbs');
                return pouchDBs.plusones;
            },
            getFingerTapDB: function() {
                console.log('getFingerTapDB triggerd in return section in initPouchDbs');
                return pouchDBs.fingertaps;
            },
            getProfileDB: function() {
                console.log('getProfileDB triggerd in return section in initPouchDbs');
                return pouchDBs.profile;
            },
            initSyncForUser: function() {
                console.log('initSyncForUser begin in return section in initPouchDbs---------------');

                var currentUser = localStorageService.get('user');

                if (!currentUser || !currentUser.id || syncActive) {
                    console.log('!currentUser || !currentUser.id || syncActive = true , initSyncForUser return&end-----------------');
                    return;
                }

                var couchUser = {
                    name: currentUser.id.toString(),
                    password: currentUser.couchPassword
                };

                var pouchOpts = {
                    skipSetup: true
                };

                var ajaxOpts = {
                    ajax: {
                        headers: {
                            Authorization: 'Basic ' + window.btoa(couchUser.name + ':' + couchUser.password)
                        }
                    }
                };

                _.forEach(databases, function(dbName) {
                    var db = new PouchDB(remoteCouchDbUrl + '/user_' + couchUser.name + '_' + dbName, pouchOpts);
                    db.login(couchUser.name, couchUser.password, ajaxOpts).then(function() {

                        var fullDbName = 'user_' + couchUser.name + '_' + dbName;

                        activeSyncs[dbName] = PouchDB.sync(fullDbName, remoteCouchDbUrl + '/' + fullDbName, {
                            live: true,
                            retry: true
                        }).on('change', function(info) {

                            //if any changes, we inform the deck
                            if (dbName === 'plusones' && (info.direction == 'pull' || info.direction == 'push') && !_.isEmpty(info.change.docs)) {
                                $rootScope.$broadcast('plusonesDBChange');
                                console.log('broadcast plusonesDBChange ----------------');
                            }

                            //if any new reminders docs to sync
                            if (dbName == 'reminders' && (info.direction == 'pull' || info.direction == 'push') && !_.isEmpty(info.change.docs)) {
                                $rootScope.$broadcast('remindersDBChange');
                                console.log('broadcast remindersDBChange ----------------');
                                console.log('pouchDBs.reminders.allDocs triggered ----------------');
                                pouchDBs.reminders.allDocs({
                                    include_docs: true
                                }).then(function(response) {
                                    console.log('lodash map reminders row begin ----------------');
                                    var reminders = _.map(response.rows, function(reminder) {
                                        return reminder.doc;
                                    });
                                    console.log('lodash map reminders row end & return reminder.doc ----------------');
                                    console.log('Call NotificationService.cancelOrphanNotifications reminders begin ----------------');
                                    NotificationService.cancelOrphanNotifications(reminders);
                                    console.log('Call NotificationService.cancelOrphanNotifications reminders end ----------------');
                                }, function(err) {
                                    console.error(err);
                                });

                                var reminders = [];
                                var newReminders = [];

                                _.each(info.change.docs, function(reminder) {
                                    if (reminder._deleted !== true) {
                                        newReminders.push(reminder);
                                    }
                                });

                                if (!_.isEmpty(newReminders)) {
                                    console.log('NotificationService.SyncNotifications is syncing changes in Reminders');
                                    NotificationService.SyncNotifications(newReminders).then(function(response) {
                                        console.log(response);
                                    }, function(err) {
                                        console.error(err);
                                    });
                                }
                            }
                        }).on('paused', function() {}).on('active', function() {}).on('denied', function(info) {}).on('complete', function(info) {}).on('error', function(err) {});
                    }).then(function(docs) {}).catch(function(error) {
                        console.error('error doing login for remote db url: ' + remoteCouchDbUrl + '/user_' + couchUser.name + '_' + dbName);
                        console.error(angular.toJson(error));
                    });
                });
                syncActive = true;
                console.log('initSyncForUser end in return section in initPouchDbs-------------------');
            },
            cancelSyncs: function() {
                console.log('cancelSyncs begin in return section in initPouchDbs------------------------');
                _.forEach(activeSyncs, function(it) {
                    it.cancel();
                });
                syncActive = false;
                console.log('cancelSyncs end in return section in initPouchDbs--------------------------');
            }
        };
    });
