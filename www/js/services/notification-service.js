angular.module('florence.services')

    .factory('NotificationService', function($q) {

        var reminderBackup = [];

        return {

            addNewPostponeNotification: function(parentNotification, uncheckedMedicines, postponeData) {

                //console.log('uncheckedMedicines', angular.toJson(uncheckedMedicines, true));
                var notification = angular.copy(parentNotification);

                //console.log('parentNotification', angular.toJson(notification, true));
                delete notification.data.medicines;
                notification.data.medicines = uncheckedMedicines;
                //date time in millisec...but the notification plugin will convert to sec
                notification.at = parseInt(moment(postponeData.hour + ':' + postponeData.minute, 'HH:mm').format('x'));
                // this will give us a one time notification
                delete notification.every;

                if (String(notification.id).charAt(8) == "1") {
                    //we already have a postpone
                } else {
                    //we add a flag '1' to indicate we have a postpone notification
                    //TODO check if the id already exist..crash android....
                    notification.id = notification.id + '1';
                }

                var notificationTitle = '';
                _.forEach(uncheckedMedicines, function(medicine) {
                    notificationTitle = notificationTitle + medicine.dosage + ' ' + medicine.tradename + ' ' + medicine.strength_group_name + '\n';
                });

                notification.title = notificationTitle;

                //console.log('New Notification', angular.toJson(notification, true));
                cordova.plugins.notification.local.schedule(notification);
            },

            /**
             * SyncNotifications: accept an array of reminders,
             * and in turn create and array of notifications to be created
             */
            SyncNotifications: function(reminders) {
                var deferred = $q.defer();

                //set the array of notifications
                var notifications = [];
                var CancelnotificationIds = [];

                angular.forEach(reminders, function(reminder) {

                    //set the message to be displayed in notifications
                    var notificationMessage = '';
                    var notificationData = {};
                    notificationData.medicines = [];
                    notificationData.id = reminder._id;
                    notificationData.category = 'medicine';
                    notificationData.hour = reminder.hour;
                    notificationData.minute = reminder.minute;

                    for (var i = 0; i < reminder.medicines.length; i++) {
                        notificationMessage = notificationMessage + reminder.medicines[i].dosage + ' ' + reminder.medicines[i].tradename + ' ' + reminder.medicines[i].strength_group_name + '\n';
                        var medicine = {};
                        medicine.nplid = reminder.medicines[i].nplid;
                        medicine.dosage = reminder.medicines[i].dosage;
                        medicine.tradename = reminder.medicines[i].tradename;
                        medicine.strength_group_name = reminder.medicines[i].strength_group_name;
                        notificationData.medicines.push(medicine);
                    }

                    //Number representing today
                    //javascript getDay() returns the day of the week (from 0 to 6 as sunday to saturday) for the specified date
                    //we want from 1 to 7 as monday to sunday
                    var now = new Date();
                    currentDay = now.getDay();
                    if (currentDay === 0) {
                        currentDay = 7;
                    }
                    //currentTime in seconds since 00:00:00
                    currentTime = ((now.getHours() * 60) + now.getMinutes()) * 60;
                    //reminderTime in seconds since 00:00:00
                    reminderTime = ((parseInt(reminder.hour) * 60) + parseInt(reminder.minute)) * 60;

                    for (var index in reminder.dayOfWeek) {

                        if (reminder.dayOfWeek[index]) {

                            //calculate the day to start the notification
                            var reminderFirstAtDay = new Date(now);

                            var offset = (index - currentDay);
                            if (offset < 0)(offset = offset + 7);
                            if ((offset === 0) && (currentTime > reminderTime)) {
                                offset = 7;
                            }
                            reminderFirstAtDay.setDate(now.getDate() + offset);

                            //debug the offset algorithm
                            //console.log('---->' + index + '\t' + (index - currentDay) + '\t' + offset);

                            //set the notification date
                            var reminderFirstAt = new Date();
                            reminderFirstAt.setFullYear(reminderFirstAtDay.getFullYear());
                            reminderFirstAt.setMonth(reminderFirstAtDay.getMonth());
                            reminderFirstAt.setDate(reminderFirstAtDay.getDate());
                            reminderFirstAt.setHours(parseInt(reminder.hour));
                            reminderFirstAt.setMinutes(parseInt(reminder.minute));
                            reminderFirstAt.setSeconds(0);

                            //alertTitle generate a warning, but thats's for the sake of the apple watch

                            notifications.push({
                                id: reminder.notificationId + index,
                                title: notificationMessage,
                                //alertTitle: notificationMessage,
                                text: '',
                                every: "week",
                                sound: ionic.Platform.isAndroid() ? 'file://beep.mp3' : 'file://beep.caf',
                                data: notificationData,
                                at: reminderFirstAt
                            });
                        } else {
                            //remove notification if dayofweek is changed to false
                            CancelnotificationIds.push(reminder.notificationId + '' + index);
                        }
                    }
                });

                if (window.cordova) {
                    cordova.plugins.notification.local.schedule(notifications, function() {
                        cordova.plugins.notification.local.cancel(CancelnotificationIds, function() {
                            deferred.resolve('Notifications not scheduled: ' + CancelnotificationIds);
                        });
                    });
                } else {
                    deferred.reject('No Cordova on your device');
                }
                return deferred.promise;
            },
            cancelOrphanNotifications: function(allreminders) {
                console.log('cancelOrphanNotifications begin --------------------');
                //Group reminders by their id
                console.log('group reminders by their id begin ------------------');
                remindersByreminder = _.groupBy(allreminders, function(reminder) {
                    return reminder._id;
                });
                console.log('group reminders by their id end ------------------');


                //Fetch local notifications
                if (window.cordova) {
                    cordova.plugins.notification.local.getAll(function(notifications) {

                        //notification.data is a string, we parse it!
                        _.forEach(notifications, function(notification, key) {
                            notifications[key].data = JSON.parse(notification.data);
                        });

                        //Group notifications by their parent reminder id
                        notificationsByreminder = _.groupBy(notifications, function(notification) {
                            return notification.data.id;
                        });

                        //Array of notifications ids that doesn't belong to a reminder
                        var ids = [];

                        //Check id a notification parent reminder id is actually a reminder
                        _.forEach(notificationsByreminder, function(reminder, key) {

                            //the reminder doesn't exist: we have to delete all notifications with this parent reminder
                            if (!remindersByreminder[key]) {
                                _.forEach(reminder, function(notification, key) {
                                    ids.push(notification.id);
                                });
                            }

                            //Postpone reminders on Android keeps getScheduled, even clicked & triggered, we delete the ones older than 15 minutes
                            _.forEach(reminder, function(notification, key) {
                                if (String(notification.id).charAt(8) == "1") {
                                    if (moment().subtract(notification.at * 1000).format('X') >= 900) {
                                        ids.push(notification.id);
                                    }
                                }
                            });

                        });
                        //Cancel the Orphan notifications
                        cordova.plugins.notification.local.cancel(ids, function() {
                            if (ids.length > 0) {
                                console.log('Orphan Notifications deleted: ', angular.toJson(ids));
                            }
                        });
                    });
                }
                console.log('cancelOrphanNotifications end --------------------');
            }
        };
    });
