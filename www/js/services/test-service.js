angular.module('florence.services')

    .factory('fingerTap', function($rootScope, $q, PouchDBService) {

        var fingerTapDB = PouchDBService.getFingerTapDB();

        $rootScope.$on('initPouchForUser', function() {
            fingerTapDB = PouchDBService.getFingerTapDB();
        });

        return {
            addSession: function(tapsession) {
                var deferred = $q.defer();
                fingerTapDB.post(tapsession).then(function(response) {
                    deferred.resolve(response);
                }, function(err) {
                    deferred.reject(err);
                });
                return deferred.promise;
            },

            getAll: function() {
                return fingerTapDB.allDocs({
                    include_docs: true
                }).then(function(response) {
                    reminders = response.rows;
                    return reminders;
                });
            },

            getSessions: function(start_date, end_date) {

                //ley's get the millisec of the date range as Int for pouchDB.query
                var start_TS = parseInt(moment(start_date).startOf('day').format('x'));
                var end_TS = parseInt(moment(end_date).endOf('day').format('x'));

                var deferred = $q.defer();

                fingerTapDB.query('by_timestamp', {
                    startkey: start_TS,
                    endkey: end_TS
                }).then(function(response) {

                    var data = response.rows;
                    deferred.resolve(data);

                }, function(err) {
                    console.log(error);
                    deferred.reject(err);
                });
                return deferred.promise;
            }
        };
    });
