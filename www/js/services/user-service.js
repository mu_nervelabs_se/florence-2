angular.module('florence.services')

    .factory('User', function($http, $q, $location, localStorageService, apiService, $state, $rootScope, PouchDBService) {

        if (localStorageService.get('user')) {
            user = localStorageService.get('user');
        }

        return {
            updateUser: function(data) {
                user = angular.extend(user, data);
                localStorageService.set('user', user);
            },
            get: function() {
                var deferred = $q.defer();
                if ((typeof user != 'undefined') && (typeof user.id != 'undefined')) {
                    deferred.resolve(user);
                } else {
                    apiService.user.current().success(function(data) {
                        if (data.id) {
                            user = data;
                            localStorageService.set('user', user);
                            PouchDBService.initSyncForUser();
                            deferred.resolve(data);
                        } else {
                            deferred.reject();
                        }
                    }).error(function() {
                        deferred.reject();
                    });
                }
                return deferred.promise;
            },
            id: function() {
                if ((typeof user != 'undefined') && (typeof user.id != 'undefined')) {
                    return user.id;
                } else {
                    return;
                }
            },
            login: function(username, password) {
                var that = this;
                var deferred = $q.defer();
                apiService.user.login({
                    password: password,
                    username: username
                }).success(function(data, status, headers, config) {
                    apiService.user.current().success(function(data) {
                        if (!data || !data.id) {
                            deferred.reject(503);
                        } else {
                            that.doAuthSuccess(data, deferred);
                        }
                    });
                }).error(function(data, status, headers, config) {
                    deferred.reject(status);
                });
                return deferred.promise;
            },

            doAuthSuccess: function(data, deferred) {
                if (typeof user != 'undefined') {
                    updatedUser = angular.extend({}, user, data);
                    user = updatedUser;
                } else {
                    user = data;
                }
                localStorageService.set('user', user);
                $rootScope.$broadcast('initPouchForUser');
                PouchDBService.initSyncForUser();
                $state.go('tab.deck');
                deferred.resolve(data);
            },
            logout: function() {
                var deferred = $q.defer();
                PouchDBService.cancelSyncs();
                apiService.user.logout()
                    .success(function() {
                        deferred.resolve();
                    })
                    .error(function() {
                        deferred.resolve();
                    });
                delete user.id;
                localStorageService.set('user', user);
                return deferred.promise;
            }
        };
    });
