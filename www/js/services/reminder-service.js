angular.module('florence.services')

    .factory('Reminders', function($rootScope, $q, PouchDBService, NotificationService) {
        var reminders = {};
        var remindersDB = PouchDBService.getReminderDB();

        $rootScope.$on('initPouchForUser', function() {
            remindersDB = PouchDBService.getReminderDB();
        });

        return {
            getAll: function(force) { // what is the mean of 'force' here ???
                var deferred = $q.defer();
                if (_.isEmpty(reminders) || force === true) {
                    remindersDB.allDocs({
                        include_docs: true
                    }).then(function(response) {
                        reminders = _.map(response.rows, function(reminder) {
                            return reminder.doc;
                        });
                        reminders = _.sortBy(reminders, function(reminder) {
                            return reminder.hour + reminder.minute; //sort by reminder time
                        });
                        deferred.resolve(reminders);
                    });
                } else {
                    deferred.resolve(reminders);
                }
                return deferred.promise;
            },

            get: function(id) {
                var defered = $q.defer();
                for (i = 0; i < reminders.length; i++) {
                    if (reminders[i]._id == id) {
                        defered.resolve(reminders[i]);
                        return defered.promise;
                    }
                }
                defered.reject('no result');
                return defered.promise;
            },

            addReminder: function(reminder) {
                var defered = $q.defer();
                if (!reminder._id) {
                    console.log('Reminders.addReminder --> CREATE reminder');
                    /**
                     * notificationId
                     * @type int(7) with the possibility to add 2 flags: weekday,postpone ex: 7493612 + 4 + 1
                     */
                    reminder.notificationId = Math.random().toString().slice(2, 9);
                    reminder.createdDate = new Date();

                    //Create a new reminder and let PouchDB generate an _id for it.
                    remindersDB.post(reminder).then(function(response) {
                        reminder._id = response.id;
                        defered.resolve(reminder);
                    }, function(err) {
                        console.log(err);
                    });
                } else {
                    console.log('Reminders.addReminder --> UPDATE reminder');
                    remindersDB.put(reminder).then(function(response) {
                        defered.resolve(reminder);
                    }, function(err) {
                        console.log(err);
                    });
                }
                return defered.promise;
            },

            deleteReminder: function(reminder) {
                var defered = $q.defer();
                remindersDB.remove(reminder)
                    .then(function(response) {
                        defered.resolve(response);
                    }, function(err) {
                        defered.reject(err);
                    });
                return defered.promise;
            },

            addNewPostponeNotification: function(parentNotification, uncheckedMedicines, postponeData) {
                NotificationService.addNewPostponeNotification(parentNotification, uncheckedMedicines, postponeData);
            }
        };
    });
