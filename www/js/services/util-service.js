angular.module('florence.services')
    .filter('numberFixedLen', function() {
        return function(a, b) {
            return (1e4 + a + "").slice(-b)
        }
    })

    .filter('dayName', function() {
        return function(input) {
            switch (input) {
                case '7':
                    return "SUNDAY";
                case '1':
                    return "MONDAY";
                case '2':
                    return "TUESDAY";
                case '3':
                    return "WEDNESDAY";
                case '4':
                    return "THURSDAY";
                case '5':
                    return "FRIDAY";
                case '6':
                    return "SATURDAY";
            }
        };
    })
