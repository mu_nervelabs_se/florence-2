'use strict';

angular.module('florence.services').factory('historyService', [
    'PouchDBService', '$q', 'Intakes', 'Reminders', 'User', '$rootScope',
    function(PouchDBService, $q, Intakes, Reminders, User, $rootScope) {


        var escapeCSVfield = function(s) {
            if (!s) {
                return '';
            }
            if (typeof s == "string") {
                return s;
            } else {
                s = String(s);
                s = s.replace(".", ",");
                return s;
            }
        };

        var b64EncodeUnicode = function(str) {
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
        };

        return {
            setLastHistoryActivityDate: function() {
                PouchDBService.getProfileDB().get('profile').then(function(data) {
                    var profile = data || {};
                    profile._id = 'profile';
                    profile.lastHistoryActivity = new Date();

                    PouchDBService.getProfileDB().put(profile).then(function() {
                        console.log("lastHistoryActivity saved in profile!");
                    });
                });
            },
            getLastHistoryActivityDate: function() {
                var deferred = $q.defer();

                PouchDBService.getProfileDB().get('profile').then(function(data) {
                    if (data.lastHistoryActivity) {
                        deferred.resolve(data.lastHistoryActivity);
                    } else {
                        console.log('%c data', 'color: deeppink; font-weight: bold; text-shadow: 0 0 5px deeppink;');
                        User.get().then(function(data) {
                            deferred.resolve(data.dateCreated);
                        });
                    }
                });

                return deferred.promise;
            },


            generateEmptyHistoryEntries: function(lastHistoryActivity) {
                var deferred = $q.defer();

                lastHistoryActivity = lastHistoryActivity || moment().subtract(7, 'days');

                var today = new Date();
                var dateArray = this.getDatesInBetweenTwoDates(lastHistoryActivity, today);

                if (!dateArray.length) {
                    deferred.resolve();
                }


                Reminders.getAll().then(function(reminders) {
                    Intakes.getIntakes().then(function(existingIntakes) {
                        _.forEach(dateArray, function(date) {
                            var dayOfWeek = moment(date).isoWeekday();

                            if (!reminders.length) {
                                deferred.resolve();
                            }

                            var countReminders = 0;
                            _.forEach(reminders, function(reminder) {
                                countReminders++;

                                var dateTimeForReminder = moment(date).hour(parseInt(reminder.hour)).minute(parseInt(reminder.minute)).second(0);

                                if (!reminder.dayOfWeek[dayOfWeek] || moment(reminder.createdDate).isAfter(date) || moment().isBefore(dateTimeForReminder)) {

                                    if (countReminders == reminders.length) {
                                        deferred.resolve();
                                    }
                                    return;
                                }


                                var newIntakesToStoreInDB = _.filter(reminder.medicines, function(medicine) {
                                    var existingIntakeForReminderAndDate = _.find(existingIntakes, function(intake) {
                                        intake = intake.doc;
                                        var intakeDate = moment.unix(intake.TS);
                                        if (intakeDate.isSame(date, 'day') && intake.reminderTime == (reminder.hour + ':' + reminder.minute) && intake.tradename == medicine.tradename) {
                                            return intake;
                                        }
                                    });

                                    if (!existingIntakeForReminderAndDate) {
                                        return medicine;
                                    }
                                });


                                if (!newIntakesToStoreInDB.length) {
                                    if (countReminders == reminders.length) {
                                        deferred.resolve();
                                    }
                                    return;
                                }

                                var countIntakes = 0;
                                _.forEach(newIntakesToStoreInDB, function(medicine) {
                                    countIntakes++;

                                    Intakes.addBasicIntakeDataFromReminder(medicine, reminder, date);
                                    medicine.isUnattended = true;

                                    Intakes.addIntake(medicine).then(function(response) {
                                        console.log(angular.toJson(response, true));

                                        if (countIntakes == newIntakesToStoreInDB.length && countReminders == reminders.length) {
                                            deferred.resolve();
                                        }
                                    });
                                });
                            });
                        });
                    });

                });

                return deferred.promise;

            },

            getDatesInBetweenTwoDates: function(startDate, stopDate) {
                var dateArray = [];
                var currentDate = startDate;
                while (moment(currentDate).isBefore(stopDate)) {
                    dateArray.push(new Date(currentDate));
                    currentDate = moment(currentDate).add(1, 'days');
                }
                return dateArray;
            },


            padListWithEmptyHistoryDays: function(list) {
                var index = 0;

                var oldestEntryDate = new Date();
                _.forEach(list, function(entry) {
                    if (entry.reminderDate < oldestEntryDate) {
                        oldestEntryDate = entry.reminderDate;
                    }
                });


                while (list.length < 7) {
                    index++;
                    list.push({
                        reminderDate: moment(oldestEntryDate).subtract(index, 'days').toDate(),
                        text: 'NO_DATA'
                    });
                }
            },


            groupIntakesByDayAndTime: function(response) {
                var groupedIntakes = [];
                var medicineIntakes = [];
                _.forEach(response, function(it) {
                    Intakes.updateOutdatedIntakeData(it);
                    medicineIntakes.push(it);
                });

                var historyDays = _.groupBy(medicineIntakes, function(it) {
                    //use of the doc.TS instead of doc.date...moment was complaining for malformated date...
                    return moment(it.doc.TS * 1000).format('YYYY-M-D'); //Group by day, disregarding differing times
                });

                _.forEach(historyDays, function(day, key) {
                    var remindersForDay = {
                        data: [],
                        differingTimes: [],
                        reminderDate: null
                    };

                    remindersForDay.reminderDate = moment(key, 'YYYY-M-D').toDate();

                    //we create a localized human readable date using moment calendar
                    remindersForDay.humanDate = moment(key, 'YYYY-M-D').calendar(null, {
                        sameDay: '[TODAY]',
                        lastDay: '[YESTERDAY]',
                        lastWeek: 'LL',
                        sameElse: 'LL'
                    });

                    var intakesGroupedByTime = _.groupBy(day, function(it) {
                        if (it.doc.kind == "Reminder") {
                            return it.doc.takenHour + ':' + it.doc.takenMinute;
                        } else {
                            return moment(it.doc.TS * 1000).format('HH:mm:ss');
                        }
                    });


                    _.forEach(intakesGroupedByTime, function(intakes, key) {
                        var intakesForTime = {
                            reminderTime: key,
                            intakes: intakes,
                            takenIntakes: _.filter(intakes, function(it) {
                                return it.doc.checked;
                            })
                        };
                        remindersForDay.data.push(intakesForTime);
                    });
                    groupedIntakes.push(remindersForDay);
                });

                return groupedIntakes;
            },

            filterSingleDocByKind: function(doc, filters) {
                if (_.find(filters, {
                        name: "Mood"
                    }).active && doc.kind == "Mood") {
                    return doc;
                } else if (_.find(filters, {
                        name: "Pain"
                    }).active && doc.kind == "Pain") {
                    return doc;
                } else if (_.find(filters, {
                        name: "Note"
                    }).active && doc.kind == "Note") {
                    return doc;
                } else if (_.find(filters, {
                        name: "Plusone"
                    }).active && doc.kind == "Plusone") {
                    return doc;
                } else if (_.find(filters, {
                        name: "Reminder"
                    }).active && doc.kind == "Reminder") {
                    return doc;
                }
            },


            exportAsCSV: function(filters) {
                var that = this;

                //headers of the csv
                var csvContent = "date" + ";" + "kind" + ";" + "name" + ";" + "strength" + ";" + "dosage" + ";" + "value\n";
                Intakes.getIntakes().then(function(response) {

                    response = _.filter(response, function(it) {
                        if (that.filterSingleDocByKind(it.doc, filters)) {
                            return it;
                        }
                    });

                    //We group the intakes by Timestamp
                    var historyTS = _.groupBy(response, function(it) {
                        return it.doc.TS; //Group by TimeStamp
                    });

                    historyTS = _.sortBy(historyTS, function(TS) {
                        return TS[0].doc.TS;
                    });

                    //We group the medicine intakes by Timestamp
                    _.each(historyTS, function(intakesdata, key) {
                        _.each(intakesdata, function(intakedata, key) {

                            var intake = intakedata.doc;

                            //CSV formating
                            var dataString = moment(intake.TS * 1000).format('YYYY-MM-DD HH:mm') + ";" + escapeCSVfield(intake.kind) + ";" + escapeCSVfield(intake.tradename) + ";" + escapeCSVfield(intake.strength_group_name) + ";" + escapeCSVfield(intake.dosage) + ";" + escapeCSVfield(intake.value) + "\n";
                            csvContent += dataString;
                        });
                    });

                    //Download options
                    if (window.cordova) {
                        cordova.plugins.email.open({
                            subject: 'Florence Export',
                            attachments: ['base64:florence-export-' + moment().format("YYYY-MM-DD") + '.csv//' + b64EncodeUnicode(csvContent)]
                        }, function(result) {
                            console.log("Response -> " + result);
                        });
                    } else {
                        //Browser case: download
                        var encodedUri = encodeURI('data:text/csv;charset=utf-8,' + csvContent);
                        window.open(encodedUri);
                    }
                });
            },

            initFiltersOnScope: function($scope) {
                $scope.filterOpen = false;

                $scope.filters = [{
                        name: 'Reminder',
                        active: true
                    },
                    {
                        name: 'Plusone',
                        active: true
                    },
                    {
                        name: 'Mood',
                        active: true
                    },
                    {
                        name: 'Pain',
                        active: true
                    },
                    {
                        name: 'Note',
                        active: true
                    }
                ];

                $scope.toggleFilter = function(filter) {
                    filter.active = !filter.active;

                    $scope.initIntakes();
                };
            },

            updateIntake: function(intake, type, value) {
                console.log('%c updateIntake', 'color: deeppink; font-weight: bold; text-shadow: 0 0 5px deeppink;');
                if (type == 'hour') {
                    intake.doc.takenHour = intake.doc.takenHour.replace(/\D+/g, '').substring(0, 2);
                    if (parseInt(intake.doc.takenHour) > 23) {
                        intake.doc.takenHour = 23;
                    }
                }
                if (type == 'minute') {
                    intake.doc.takenMinute = intake.doc.takenMinute.replace(/\D+/g, '').substring(0, 2);
                    if (parseInt(intake.doc.takenMinute) > 59) {
                        intake.doc.takenMinute = 59;
                    }
                }

                if (type == 'takenDate' && value) {
                    intake.doc.TS = moment(value).unix();
                    intake.doc.date = moment(value).date();
                }

                intake.doc.isUnattended = false;

                Intakes.updateIntake(intake).then(function(response) {
                    intake.doc._rev = response.rev;
                });
            },

            deleteIntakeFromList: function(intake, list) {
                var index = list.indexOf(intake);
                list.splice(index, 1);
                Intakes.deleteIntake(intake).then(function() {
                    if (list.length === 0) {
                        $rootScope.$broadcast('showFrontPanel');
                    }
                });
            },


            deleteIntake: function(callback, intake) {
                Intakes.deleteIntake(intake).then(function() {
                    (callback || angular.noop)();
                    $rootScope.$broadcast('showFrontPanel');
                });
            },


            addTestIntake: function(date, hour, minute) {
                var intake = {
                    "nplid": "19850613000175",
                    "tradename": "Vitalipid® adult",
                    "strength_group_name": "(Grupp A)",
                    "pharmaceutical_form_group_name": "Koncentrat till infusionsvätska, emulsion",
                    "organization": "Fresenius Kabi AB",
                    "dosage": 1,
                    "checked": true
                };

                var notification = {
                    "userid": 1,
                    "hour": hour,
                    "minute": minute
                };

                Intakes.addBasicIntakeData(intake, notification);
                intake.date = date;
                Intakes.addIntake(intake);

                intake.tradename = "Madopark®";
                Intakes.addIntake(intake);

                $scope.initIntakes();
            },

            deleteAllIntakes: function() {
                Intakes.getIntakes().then(function(response) {
                    _.forEach(response, function(intake) {
                        Intakes.deleteIntake(intake.doc);
                    });
                    $scope.groupedIntakes = [];
                });
            }
        };
    }
]);
