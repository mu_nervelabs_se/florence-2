angular.module('florence', [
    'ionic', 'florence.controllers', 'florence.translate', 'florence.services', 'florence.directives', 'florence.api',
    'angularMoment', 'ngCordova', 'LocalStorageModule', 'angucomplete-alt', 'chart.js', 'ngSanitize', 'nvd3', 'angular-svg-round-progress'
  ])

  .run(function($ionicPlatform, $rootScope, $translate, PouchDBService) {

    $ionicPlatform.ready(function() {
      console.log("");
      console.log("************************************************************");
      console.log("*                      FLORENCE                            *");
      console.log("************************************************************");
      console.log('');
      console.log("ionicPlatform is ready--------------------------------------");

      // getPreferredLanguage: We use the prefered language of the device for our translation file, and the moment lib
      console.log('localization process begin ---------------------------------');
      if (typeof navigator.globalization !== "undefined") {
        navigator.globalization.getPreferredLanguage(function(language) {
          moment.locale((language.value).split("-")[0]);
          $translate.use((language.value).split("-")[0]).then(function(data) {

            //use of the locale to get a default medicineDB value
            if (data == 'en') {
              $rootScope.medicineDB = 'us';
              $rootScope.classLanguageCode = 'english';
            }
            if (data == 'sv') {
              $rootScope.medicineDB = 'se';
              $rootScope.classLanguageCode = 'swedish';
            }

          }, function(error) {
            console.error("getPreferredLanguage ERROR: " + error);
          });
        }, null);
      } else {
        //Fallback to English and us medicineDB
        moment.locale('en');
        $translate.use('en');
        $rootScope.medicineDB = 'us';
        $rootScope.classLanguageCode = 'english';
      }
      console.log('localization process end ---------------------------------');

      if (window.cordova) {
        console.log('window.cordova in app.js begin ---------------------------------');
        console.log('version and build update begin ---------------------------------');
        // This will be updated at build time by db_update.js hook
        $rootScope.version = '%VERSION%';
        $rootScope.build = '%BUILD%';
        console.log('version and build update end ---------------------------------');

        console.log('notification.local.on.click begin ----------------------------');
        cordova.plugins.notification.local.on("click", function(notification, state) {
          console.log('Notification Clicked state ' + state);
          $rootScope.notificationProperties = notification;
          $rootScope.$broadcast('notificationClick');
        });
        console.log('notification.local.on.click end   ----------------------------');

        console.log('notification.local.getScheduled begin -------------------------');
        cordova.plugins.notification.local.getScheduled(function(notifications) {
          notifications = _.sortBy(notifications, 'id');
          _.forEach(notifications, function(notification) {
            console.log("Scheduled " + " " + moment(notification.at * 1000).format('MM-DD H:mm') + " " + notification.id + " " + notification.title.replace(/(?:\r\n|\r|\n)/g, ''));
          });
        });
        console.log('notification.local.getScheduled end --------------------------');

        console.log('notification.local.getTriggered begin ------------------------');
        cordova.plugins.notification.local.getTriggered(function(notifications) {
          notifications = _.sortBy(notifications, 'id');
          _.forEach(notifications, function(notification) {
            console.log("Triggered" + " " + moment(notification.at * 1000).format('MM-DD H:mm') + " " + notification.id + " " + notification.title.replace(/(?:\r\n|\r|\n)/g, ''));
          });
        });
        console.log('notification.local.getTriggered end ------------------------');
        console.log('window.cordova in app.js end ---------------------------------');
      }

      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar && ionic.Platform.isAndroid()) {
        StatusBar.hide();
      }
      console.log('Call PouchDBService.initSyncForUser begin in app.js ---------------');
      PouchDBService.initSyncForUser();
      console.log('Call PouchDBService.initSyncForUser end in app.js ---------------');
    });
  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.views.transition('android');

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html",
        controller: "TabsCtrl"
      })
      /*
      Auth tab views
      */
      .state('tab.auth', {
        url: '/auth',
        views: {
          'tab-auth': {
            templateUrl: 'templates/tab-auth.html',
            controller: 'AuthCtrl'
          }
        },
        resolve: {
          currentUser: function(User, $state) {
            if (User.id()) {
              $state.transitionTo('tab.deck');
            } else {
              return null;
            }
          }
        }
      })
      .state('tab.auth-login', {
        url: '/auth/login',
        views: {
          'tab-auth': {
            templateUrl: 'templates/auth-login.html',
            controller: 'AuthCtrl'
          }
        }
      })
      .state('tab.auth-signup', {
        url: '/auth/signup',
        views: {
          'tab-auth': {
            templateUrl: 'templates/auth-signup.html',
            controller: 'AuthCtrl'
          }
        }
      })
      .state('tab.forgot-password', {
        url: '/auth/forgot-password',
        views: {
          'tab-auth': {
            templateUrl: 'templates/auth-forgot-password.html',
            controller: 'AuthCtrl'
          }
        }
      })
      /*
      Deck tab views
      */
      .state('tab.deck', {
        url: '/deck',
        views: {
          'tab-deck': {
            templateUrl: 'templates/tab-deck.html',
            controller: 'DeckCtrl'
          }
        }
      })
      .state('tab.reminder', {
        url: '/reminders/:reminderId',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-reminder.html',
            controller: 'ReminderCtrl'
          }
        }
      })
      .state('tab.plusone-add', {
        url: '/plusone/add',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone-add.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.plusone', {
        url: '/plusone/:plusoneId',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.food', {
        url: '/food',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone-food.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.mood', {
        url: '/mood',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone-mood.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.pain', {
        url: '/pain',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone-pain.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.note', {
        url: '/note',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-plusone-note.html',
            controller: 'PlusoneCtrl'
          }
        }
      })
      .state('tab.service-add', {
        url: '/service-add',
        views: {
          'tab-deck': {
            templateUrl: 'templates/service-add.html',
            controller: 'ServiceCtrl'
          }
        }
      })
      .state('tab.service', {
        url: '/service/:service',
        views: {
          'tab-deck': {
            templateUrl: 'templates/service-add.html',
            controller: 'ServiceCtrl'
          }
        }
      })
      .state('tab.class', {
        url: '/classes/:classId',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-class.html',
            controller: 'ClassCtrl'
          }
        }
      })
      .state('tab.profile', {
        url: '/system/profile',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-profile.html',
            controller: 'ProfileCtrl'
          }
        }
      })
      .state('tab.profile-2', {
        url: '/system/profile-2',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-profile-2.html',
            controller: 'ProfileCtrl-2'
          }
        }
      })
      .state('tab.contact', {
        url: '/system/contact',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-contact.html',
            controller: 'ContactCtrl'
          }
        }
      })
      .state('tab.chart-medication', {
        url: '/charts/medication',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-chart-medicine.html',
            controller: 'MedicineChartCtrl'
          }
        }
      })
      .state('tab.chart-sleep', {
        url: '/charts/sleep',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-chart-sleep.html',
            controller: 'SleepChartCtrl'
          }
        }
      })
      .state('tab.chart-activity', {
        url: '/charts/activity',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-chart-activity.html',
            controller: 'ActivityChartCtrl'
          }
        }
      })
      .state('tab.chart-taptest', {
        url: '/charts/taptest',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-chart-taptest.html',
            controller: 'TaptestChartCtrl'
          }
        }
      })
      .state('tab.taptest', {
        url: '/tap-test',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-tap-test.html',
            controller: 'fingerTapCtrl'
          }
        }
      })
      .state('tab.history', {
        url: '/history',
        views: {
          'tab-deck': {
            templateUrl: 'templates/deck-history.html',
            controller: 'HistoryCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/auth');
  });

Chart.defaults.global = {
  // Boolean - Whether to animate the chart
  animation: false,

  // Number - Number of animation steps
  animationSteps: 45,

  // String - Animation easing effect
  // Possible effects are:
  // [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
  //  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
  //  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
  //  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
  //  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
  //  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
  //  easeOutElastic, easeInCubic]
  animationEasing: "easeOutBounce",

  // Boolean - If we should show the scale at all
  showScale: false,

  // Boolean - If we want to override with a hard coded scale
  scaleOverride: false,

  // ** Required if scaleOverride is true **
  // Number - The number of steps in a hard coded scale
  scaleSteps: null,
  // Number - The value jump in the hard coded scale
  scaleStepWidth: null,
  // Number - The scale starting value
  scaleStartValue: null,

  // String - Colour of the scale line
  scaleLineColor: "rgba(0,0,0,1)",

  // Number - Pixel width of the scale line
  scaleLineWidth: 1,

  // Boolean - Whether to show labels on the scale
  scaleShowLabels: false,

  // Interpolated JS string - can access value
  scaleLabel: "<%=value%>",

  // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
  scaleIntegersOnly: true,

  // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
  scaleBeginAtZero: false,

  // String - Scale label font declaration for the scale label
  scaleFontFamily: "RobotoCondensed-Regular",

  // Number - Scale label font size in pixels
  scaleFontSize: 12,

  // String - Scale label font weight style
  scaleFontStyle: "normal",

  // String - Scale label font colour
  scaleFontColor: "#666",

  // Boolean - whether or not the chart should be responsive and resize when the browser does.
  responsive: true,

  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio: true,

  // Boolean - Determines whether to draw tooltips on the canvas or not
  showTooltips: true,

  // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
  customTooltips: false,

  // Array - Array of string names to attach tooltip events
  tooltipEvents: ["mousemove", "touchstart", "touchmove"],

  // String - Tooltip background colour
  tooltipFillColor: "rgba(255,255,255,0.8)",

  // String - Tooltip label font declaration for the scale label
  tooltipFontFamily: "RobotoCondensed-Regular",

  // Number - Tooltip label font size in pixels
  tooltipFontSize: 14,

  // String - Tooltip font weight style
  tooltipFontStyle: "normal",

  // String - Tooltip label font colour
  tooltipFontColor: "#7A12D6",

  // String - Tooltip title font declaration for the scale label
  tooltipTitleFontFamily: "RobotoCondensed-Regular",

  // Number - Tooltip title font size in pixels
  tooltipTitleFontSize: 14,

  // String - Tooltip title font weight style
  tooltipTitleFontStyle: "bold",

  // String - Tooltip title font colour
  tooltipTitleFontColor: "#7A12D6",

  // Number - pixel width of padding around tooltip text
  tooltipYPadding: 6,

  // Number - pixel width of padding around tooltip text
  tooltipXPadding: 6,

  // Number - Size of the caret on the tooltip
  tooltipCaretSize: 8,

  // Number - Pixel radius of the tooltip border
  tooltipCornerRadius: 6,

  // Number - Pixel offset from point x to tooltip edge
  tooltipXOffset: 10,

  // String - Template string for single tooltips
  tooltipTemplate: "<%= value %>",

  // String - Template string for multiple tooltips
  multiTooltipTemplate: "<%= value %>",

  // Function - Will fire on animation progression.
  onAnimationProgress: function() {},

  // Function - Will fire on animation completion.
  onAnimationComplete: function() {}
};
